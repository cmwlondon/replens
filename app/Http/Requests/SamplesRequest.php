<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class SamplesRequest extends FormRequest {

	public function __construct() {}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {

		$rules = [
			'title'						=> 'required|max:255',
			'firstname'				=> 'required|max:255',
			'lastname'   			=> 'required|max:255',
			'email'						=> 'required|email',
			'phone'						=> 'required|min:5|max:20',
			'jobTitle'    		=> 'required|max:255',
			'practiceName'    => 'required|max:255',
			'practiceAddress' => 'required|max:255',
			'patients'				=> 'required',
			'optIn'						=> '',
			'captcha'				=> 'required|captcha'
		];

		return $rules;
	}

	public function messages()
	{
	    return [
	    ];
	}
}

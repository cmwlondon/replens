<?php namespace App\Http\Controllers\Admin\Summary;

use App\Http\Controllers\Admin\AdminController;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Response;
use Config;
use Request;
use DB;
use Carbon\Carbon;
use Excel;
use File;

use App\Models\Samples;

class SummaryController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/


	protected $filename;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Guard $guard)
	{
		parent::__construct($guard);

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['title'] = 'System Summary';

		// $this->context['pageViewJS'] = 'admin/summary-min';
		$this->context['pageViewJS'] = '';

		$samples_summary = DB::table('samples')
                     ->select(DB::raw('`patients` , COUNT(  `patients` ) AS  `count`'))
                     ->groupBy('patients')
                     ->orderBy('count','desc')
                     ->get();
    	$this->context['samples_summary'] = $samples_summary;

		return view('admin.summary.home', $this->context);
	}

	public function samples_csv() {

		$now = Carbon::now('Europe/London')->format('dmY-His');

		$samples = Samples::all();
		// the csv file with the first row
    	$output = '"'.implode('","', array('Id', 'Title', 'First name', 'Last name', 'Email', 'Job Title', 'Practice Name', 'Practice Address', 'Patients', 'Opt In', 'Date')) .'"'. "\n";

    	foreach ($samples as $row) {
       		// iterate over each sample and add it to the csv
	        $output .=  '"'.implode('","', array($row['id'], $row['title'], $row['firstname'], $row['lastname'], $row['email'], $row['jobTitle'], $row['practiceName'], $row['practiceAddress'], $row['patients'], $row['optIn'], $row['created_at'])) .'"'. "\n"; // append each row
	    }

	    $headers = array(
        	'Content-Type' => 'text/csv',
        	'Content-Disposition' => 'attachment; filename="samples_'.$now.'.csv"',
        );

        return Response::make(rtrim($output, "\n"), 200, $headers);
	}
}

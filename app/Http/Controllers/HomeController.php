<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->context = array(
            'pageViewCSS'       => "",
            'pageViewJS'        => "",
            'site_url'          => url('',[]),
            'class_section' => '',
            'meta'              => [
                                    'title' => 'laravel56 test'
                                    ]
        );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', $this->context);
    }

    public function delta()
    {
        // perform direct DB query, don't use model
        // SELECT patients, COUNT(patients) AS n FROM samples GROUP BY patients ORDER BY n DESC
        $samples_summary = DB::table('samples')
                     ->select(DB::raw('`patients` , COUNT(  `patients` ) AS  `count`'))
                     ->groupBy('patients')
                     ->orderBy('count','desc')
                     ->get();
        $this->context['samples_summary'] = $samples_summary;
        
        return view('admin.delta', $this->context);
    }

    public function gamma()
    {
        return view('admin.gamma', $this->context);
    }
}

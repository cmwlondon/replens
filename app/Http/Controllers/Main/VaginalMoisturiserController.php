<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class VaginalMoisturiserController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/moisturiser.min';
		$this->context['pageViewCSS']	= 'main/sections/moisturiser';

		$this->context['meta']['title']	= 'ReplensMD&trade; Vaginal Moisturiser - Resolve Dryness Symptoms';
		$this->context['meta']['desc']	= 'Are you experiencing vaginal dryness symptoms? Put an end to itching, discomfort, soreness and irritation with ReplensMD&trade; vaginal moisturiser today!';

		return view('main.moisturiser.home', $this->context);
	}
}

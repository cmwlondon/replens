<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Services\SitemapPackager;

class MiscController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	public function sitemap(SitemapPackager $packager)
	{

		return $packager->create();
	}
}

<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Samples;
use App\Http\Requests\SamplesRequest;
use App;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProfessionalsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{


		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser - Professionals';
		$this->context['meta']['desc']	= 'Help your patients wave bye-bye to vaginal dryness and hello to happier days to come';
		$this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];
		$this->context['patientsArr'] = ['0-3' => '0-3', '3-6' => '3-6', '6-9' => '6-9', '9-12' => '9-12', '12-15' => '12-15', '15+' => '15+'];
		$this->context['pageViewJS']	= 'main/professionals.min';
		$this->context['pageViewCSS']	= 'main/sections/professionals';

		return view('main.professionals.home', $this->context);
	}

	public function store(SamplesRequest $request, Samples $samples)
	{
		$samples->fill($request->all());
		$samples->save();

		// Prepare the email to send.
		$details = array(
			'title'						=> $request->title,
			'firstname'				=> $request->firstname,
			'lastname'				=> $request->lastname,
			'address'					=> $request->address,
			'email'						=> $request->email,
			'phone'						=> $request->phone,
			'jobTitle'				=> $request->jobTitle,
			'practiceName'		=> $request->practiceName,
			'practiceAddress'	=> $request->practiceAddress,
			'patients'				=> $request->patients,
			'optIn'						=> $request->optIn
		);

		// if(getenv('APP_ENV') === 'production' || getenv('APP_ENV') === 'staging'){
			$this->sendSYsMail($details);
		// }

		$this->response = array('response_status' => 'success', 'message' => '<h1>Thank you we have recieved your request for samples.</h1><p>We will get them in the post soon!</p>');

		return redirect('professionals#thanks')->with('response', $this->response);
	}

	public function download($asset)
	{
		if ($asset == 'leaflet') {
			// return response()->download('pdfs/replens-gp-leaflet-29102019.pdf', 'Replens GP Leaflet.pdf');
			
			return response()->download('pdfs/replens_education_leaflet-10-11-2020.pdf', 'Replens Education Leaflet 10th November 2020.pdf');
		}
	}

	private function sendSYsMail($details)
	{

		Mail::send(['text' => 'emails.sample'], $details, function($message) use($details)
		{
		    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
		    $message->to(env('MAIL_ADMIN_TO'));
			// $message->bcc(env('MAIL_ADMIN_BCC'));
		    $message->subject('Replens Sample Request');
		});
	}
}

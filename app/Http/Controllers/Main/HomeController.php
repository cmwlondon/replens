<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/home.min';
		$this->context['pageViewCSS']	= ''; //'main/sections/home';
		
		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser - Vaginal Atrophy & Dryness Relief';
		$this->context['meta']['desc']	= 'ReplensMD is the UK&rsquo;s number one GP recommended brand for vaginal dryness. Get immediate alleviation of dryness symptoms and replenish vaginal moisture.';

		return view('main.home.home', $this->context);
	}
}

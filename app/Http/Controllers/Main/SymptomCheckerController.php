<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App;

class SymptomCheckerController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['meta']['title']	= 'ReplensMD&trade; - Vaginal Dryness Symptom Checker';
		$this->context['meta']['desc']	= 'Irritated, itchy, burning or sore vagina? Try Replens symptom checker to help diagnose if you could be suffering from vaginal dryness.';
		$this->context['meta']['keywords']	= 'irritated,irritation,itchy,itching,burning,burns,sore,painful,sex,vagina,vaginal,dryness,dry';
		$this->context['meta']['keyphrases']	= 'itchy vagina, sore vagina, vagina burns, painful vagina, painful sex';
		$this->context['pageViewJS']	= 'main/symptom-checker.min';
		$this->context['pageViewCSS']	= 'main/sections/symptom-checker';

		return view('main.symptom-checker.home', $this->context);
	}
}

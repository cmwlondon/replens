<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Samples;
use App\Http\Requests\SamplesRequest;
use App;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SNGOController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser - Sex Never Gets Old';
		$this->context['meta']['desc']	= '';
		$this->context['pageViewJS']	= 'main/sngo.min';
		$this->context['pageViewCSS']	= 'main/sections/sngo';

		return view('main.sngo.home', $this->context);
	}

}

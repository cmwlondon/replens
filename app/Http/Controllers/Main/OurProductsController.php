<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class OurProductsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/products.min';
		$this->context['pageViewCSS']	= 'main/sections/products';

		$this->context['meta']['title']	= 'Replens Moisturiser Products - Oestrogen & Fragnance Free';
		$this->context['meta']['desc']	= 'Replens MD&trade; is a clinically tested product, helping replenish moisture to dry vaginal cells by supplementing the body\'s natural lubrication. Find out more!';

		return view('main.products.home', $this->context);
	}
}

<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Contact;
use App\Http\Requests\ContactRequest;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ContactUsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/contact';

		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser - Website and Product Enquiries';
		$this->context['meta']['desc']	= 'If you have any comments or questions, feel free to use our website contact form inside. Fill all required fields and we\'ll get back to you shortly.';

		$this->context['titlesArr'] = ['' => 'Select', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];

		return view('main.contact.home', $this->context);
	}

	public function store(ContactRequest $request, Contact $contact)
	{

		$details = array(
			'title'			=> $request->title,
			'firstname'		=> $request->firstname,
			'lastname'		=> $request->lastname,
			'address'		=> '-',//$request->address,
			'city'			=> '-',//$request->city,
			'postcode'		=> '-',//$request->postcode,
			'country'		=> '-',//$request->country,
			'phone'			=> $request->phone,
			'email'			=> $request->email,
			'product'		=> $request->product,
			'comments'		=> $request->comments
		);

		$contact->fill($details);
		$contact->save();

		if(getenv('APP_ENV') === 'production' || getenv('APP_ENV') === 'staging'){
			$this->sendSYsMail($details);
			// $this->sendMail($details);
		}

		$this->response = array('response_status' => 'success', 'message' => '<h1>Thank you for your enquiry.</h1> <p>Our team will be in touch with you soon.</p>');

		return redirect('contact-us')->with('response', $this->response);
	}


	private function sendMail($details)
	{
		Mail::send('emails.enquiry', $details, function($message) use($details)
		{
		    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
		    $message->to(env('MAIL_ADMIN_TO'));

		    if (!is_null(env('MAIL_ADMIN_CC')) && env('MAIL_ADMIN_CC') != "") {
		    	$message->cc(env('MAIL_ADMIN_CC'));
		    }
		    if (!is_null(env('MAIL_ADMIN_BCC')) && env('MAIL_ADMIN_BCC') != "") {
		    	$message->bcc(env('MAIL_ADMIN_BCC'));
		    }
		    $message->subject('Replens Enquiry');
		});
	}

	private function sendSYsMail($details)
	{
		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
		    $message->from(env('MAIL_NOREPLY'), env('MAIL_NAME'));
		    $message->to(env('MAIL_SYS_TO'));
			$message->bcc(env('MAIL_ADMIN_BCC'));
		    $message->subject('UK ChurchDwight Enquiry');
		});
	}

	public function mailtest()
	{

		// Prepare the email to send.
		$details = array(
			'title'				=> 'TITLE',
			'firstname'		=> 'firstname',
			'lastname'		=> 'lastname',
			'company'			=> '',//$request->company,
			'address'			=> '',//$request->address,
			'address_2'			=> '',//$request->address_2,
			'city'				=> '',//$request->city,
			'state'				=> '',//$request->county,
			'zip'				=> '',//$request->postcode,
			'country'			=> '',//$request->country,
			'phone'				=> 'phone',
			'email'				=> 'email',
			'jobTitle'			=> '',
			'product'			=> 'product',
			'comments'		    => 'comments',
			'mfgcode'			=> 'mfgcode'
		);

		$this->sendSYsMail($details);
	}

}

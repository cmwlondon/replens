<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class VaginalDrynessController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->context['pageViewJS']	= 'main/vaginal.min';
		$this->context['pageViewCSS']	= 'main/sections/vaginal';

		$this->context['meta']['title']	= 'What is Vaginal Dryness - Causes & Effective Treatments';
		$this->context['meta']['desc']	= 'Vaginal dryness is a common issue which can occur at any age. Luckily, Replens MD&trade; can help you effectively reduce the negative impact it has on your life.';

		return view('main.vaginal.home', $this->context);
	}
}

<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PoliciesController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 *
	 * @return Response
	 */
	public function terms()
	{
		$this->context['pageViewJS']	= 'main/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/teesncees';

		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser Website Terms & Conditions';
		$this->context['meta']['desc']	= 'The following terms and conditions (the &ldquo;Terms and Conditions&rdquo;) govern your use of this web site or application, and any content made available from or through.';

		return view('main.policies.terms', $this->context);
	}

	/**
	 *
	 * @return Response
	 */
	public function cookies()
	{
		$this->context['pageViewJS']	= 'main/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/teesncees';

		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser Website Cookie Notice';
		$this->context['meta']['desc']	= '';

		return view('main.policies.cookies', $this->context);
	}

	/**
	 *
	 * @return Response
	 */
	public function privacy()
	{
		$this->context['pageViewJS']	= 'main/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/teesncees';

		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser Website Privacy Policy';
		$this->context['meta']['desc']	= '';

		return view('main.policies.privacy', $this->context);
	}

	/**
	 *
	 * @return Response
	 */
	public function thirdparty()
	{
		$this->context['pageViewJS']	= 'main/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/teesncees';

		$this->context['meta']['title']	= 'ReplensMD&trade; Third Party Information Collection';
		$this->context['meta']['desc']	= '';

		return view('main.policies.third-party', $this->context);
	}

	public function mailtest()
	{
		$this->context['pageViewJS']	= 'main/contact.min';
		$this->context['pageViewCSS']	= 'main/sections/teesncees';

		$this->context['meta']['title']	= 'mailtest';
		$this->context['meta']['desc']	= '';

		$this->context['time'] = date("Y-m-d H:i:s");

		$details = array(
			'title'			=> 'Mr',
			'firstname'		=> 'Albert',
			'lastname'		=> 'Einstein',
			'phone'			=> '7542087301',
			'email'			=> 'albert.einstain@gmail.com',
			'product'		=> 'replens mailgun test product',
			'comments'		=> 'replens meilgun test comment',
			'mfgcode'		=> 'sentient.replens.088',
			'timestamp' 	=> date("Y-m-d H:i:s"),
			'alpha' => '',
			'beta' => '',
			'deltaa' => '',
			'gamma' => '',
			'omega' => '',
			'epsilon' => '',
			'phi' => ''
		);

		// /resources/views/emails/enquiry.blade.php
		Mail::send(['text' => 'emails.enquiry'], $details, function($message) use($details)
		{
			$message->from('replens@sandbox677aa5c46b624036a28a2e15eb445c02.mailgun.org', 'replens');
			$message->to('hockey.richard@gmail.com'); // address in list of authorised test addresses for sandbox
		    $message->subject('UK ChurchDwight Enquiry');
		});

		return view('main.policies.mailtest', $this->context);
	}
}

<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Session;
use Request;

class MainController extends Controller {
	
	public function __construct() {
		
		$this->context['section'] = "";

		$this->context = array(
			'pageViewCSS' 		=> "",
			'pageViewJS'		=> "",
			'site_url'			=> url('',[]),
			'class_section'	=> '',
			'meta'				=> [
									'title' => 'ReplensMD&trade; Moisturiser',
									'link' => url(Request::path()),
									'pagetype' => 'article',
									'desc' => '',
									'image' => url('/images/replens-logo.png')
									]
		);
	}
}
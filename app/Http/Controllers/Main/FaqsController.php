<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class FaqsController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function productFaqs()
	{
		$this->context['pageViewJS']	= 'main/faqs.min';
		$this->context['pageViewCSS']	= 'main/sections/faqs';

		$this->context['meta']['title']	= 'ReplensMD&trade; Moisturiser FAQ - What is It & How It Works';
		$this->context['meta']['desc']	= 'Answers to all your questions about Replens MD&trade; - does it have side effects, how it works, when and how often to use it. Get all information inside.';

		return view('main.faqs.product_faqs', $this->context);
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function expertFaqs()
	{
		$this->context['pageViewJS']	= 'main/faqs.min';
		$this->context['pageViewCSS']	= 'main/sections/faqs';

		return view('main.faqs.expert_faqs', $this->context);
	}
}

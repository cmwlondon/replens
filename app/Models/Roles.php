<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Roles extends Model {


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	*/
	protected $fillable = [
		'title',
		'ref',
		'level'
	];

	/**
     * Get the permissions a user has
     */
    public function permissions()
    {	
        return $this->belongsToMany('App\Models\Permissions', 'permissions_roles');
    }

	//
	public function linkPermissions($permissions) {
		$linked_permissions = DB::table('permissions')
					->select('id')
                    ->whereIn('name', $permissions)
                    ->get();
		$this->permissions()->attach(array_pluck($linked_permissions, 'id'));
	}
}

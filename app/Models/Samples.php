<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Samples extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
		protected $table = 'samples';

		public $timestamps = true;

		/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		 'title'
		,'firstname'
		,'lastname'
		,'email'
		,'phone'
		,'jobTitle'
		,'practiceName'
		,'practiceAddress'
		,'patients'
		,'optIn'
	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

}

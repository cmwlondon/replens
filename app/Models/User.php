<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'roles_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public function assignRole($ref) {
		$role = Roles::where('ref', $ref)->firstOrFail();

		$this->roles_id = $role->id;
		$this->save();
	}

	/**
     * Get the role a user belongs to
     */
    public function role()
    {	
        return $this->hasOne('App\Models\Roles', 'id', 'roles_id');
    }


	/**
     * Get the permissions a user has
     */
    public function permissions()
    {	
        return $this->belongsToMany('App\Models\Permissions', 'permissions_roles', 'roles_id', 'permissions_id');
    }
}

<?php namespace App\Services;

use Response;
use Carbon\Carbon;
use Storage;

use LSS\Array2XML;


class SitemapPackager {

	public function __construct()
	{
		Carbon::setLocale('en');

		$this->sitemap = [
			'@attributes' => [
				'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd',
				'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9'
			],

			'url' => []
		];


	}

	public function create() {
		/* */
		$now = new Carbon();
		$now = $now->toW3cString();

		// Home page
		$this->addUrl(url('/'),$now,'monthly','1.0');
		/*
		bug with the original implemantation of the line
		url() -> returns an object instaed of a string, breaking this entire method
		use url('/') instead -> returns the base URL of the site
		*/

		$this->addUrl(url('/vaginal-dryness'),$now,'monthly','0.9');
		$this->addUrl(url('/vaginal-moisturiser'),$now,'monthly','0.9');
		$this->addUrl(url('/our-products'),$now,'monthly','0.9');
		$this->addUrl(url('/product-faqs'),$now,'monthly','0.9');
		$this->addUrl(url('/terms-and-conditions'),$now,'monthly','0.9');
		$this->addUrl(url('/cookie-notice'),$now,'monthly','0.9');
		$this->addUrl(url('/privacy-policy'),$now,'monthly','0.9');
		$this->addUrl(url('/symptom-checker'),$now,'monthly','0.9');
		$this->addUrl(url('/contact-us'),$now,'monthly','0.9');
		$this->addUrl(url('/professionals'),$now,'monthly','0.9');

		// Convert and save
		$xml = Array2XML::createXML('urlset', $this->sitemap);

		$response = $xml->saveXML();
		//dd(Storage::disk('public'));
		Storage::disk('public')->put('sitemap.xml', $response);
		/* */

		$response = Storage::disk('public')->get('sitemap.xml'); // /storage/app/public/sitemap.xml
		
		return Response::make($response, '200')->header('Content-Type', 'text/xml');
	}

	private function addUrl($url,$lastmod,$freq,$priority) {
		$this->sitemap['url'][] = ['loc' => $url,
							'lastmod' => $lastmod,
							'changefreq' => $freq,
							'priority' => $priority
							];
	}
}

<?php namespace App\Services;

use Illuminate\Contracts\Auth\Guard as Guard;
use App;
use Illuminate\Support\Facades\Auth;
use App\Models\User as User;

class AdminServices {
		
	protected $permissions = null;
	protected $auth;

	public function __construct() {
	}
	/*
	public function __construct() {
        $this->user = User::find( Auth::id() );
        $this->permissions = $this->user->role->permissions;
	}
	*/

	// public function register(Guard $guard) {
	public function register() {
	}

	public function getAuthUser() {
		$this->user = User::find( Auth::id() );
		if ($this->user != null) {
			$this->permissions = $this->user->role->permissions;
		}
		return '';
	}

	public function user($key) {
		if ($this->user != null) {
			return $this->user[$key];
		}
		return '';
	}

	public function checkPermission($key) {
		//dd($this->permissions);
		if ($this->permissions != null) {
			foreach ($this->permissions->toArray() as $item)
			{
			    if ($item['name'] == $key) {
			    	return true;
			    }
			}
		}
		return false;
	}

	public function checkPagePermission($key) {
		if ($this->checkPermission($key) == false) {
			view()->share('error','Access Denied');
			App::abort(401);
		}
	}
}
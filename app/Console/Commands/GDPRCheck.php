<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Contact;
use App\Models\Samples;

use Carbon\Carbon;

class GDPRCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     protected $name = 'gdpr:check';
    //protected $signature = 'gdpr:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks supports table for entries over 3 months old and obscures the data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Contact $contact, Samples $samples)
    {
        echo "gdpr:check";

        // get records older than 3 months
        $data = $contact->where('firstname','<>','GDPR')->whereDate('created_at', '<=', Carbon::today()->subMonths(3)->toDateString() )->get();
        foreach($data as $key => $entry) {
            $entry->title = '-';
            $entry->firstname = 'GDPR';
            $entry->lastname = '-';
            $entry->address = '-';
            $entry->city = '-';
            $entry->postcode = '-';
            $entry->country = '-';
            $entry->phone = '-';
            $entry->email = $entry->created_at->timestamp . '@gdpr.com';
            $entry->product = '-';
            $entry->comments = '-';
            // save uodated record
            $entry->save();
            // output ids of amended record
            $this->line($key);

        }

        // sample request / professionals
        $data = $samples->where('firstname','<>','GDPR')->whereDate('created_at', '<=', Carbon::today()->subMonths(3)->toDateString() )->get();
        foreach($data as $key => $entry) {
            $entry->title = '-';
            $entry->firstname = 'GDPR';
            $entry->lastname = '-';
            $entry->email = $entry->created_at->timestamp . '@gdpr.com';
            $entry->phone = '-';
            $entry->jobTitle = '-';
            $entry->practiceName = '-';
            $entry->practiceAddress = '-';
            $entry->patients = '-';
            $entry->optin = '0';

            $entry->save();
            // output ids of amended record
            $this->line($key);
        }

        return 0;
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// public
Route::get('/','Main\HomeController@index');
Route::get('/vaginal-dryness', 			'Main\VaginalDrynessController@index');
Route::get('/vaginal-moisturiser', 	'Main\VaginalMoisturiserController@index');
Route::get('/our-products', 				'Main\OurProductsController@index');
Route::get('/product-faqs', 				'Main\FaqsController@productFaqs');
Route::get('/terms-and-conditions', 'Main\PoliciesController@terms');
Route::get('/cookie-notice',        'Main\PoliciesController@cookies');
Route::get('/privacy-policy',        'Main\PoliciesController@privacy');

//Route::get('/mailtest',        'Main\PoliciesController@mailtest');

Route::get('/symptom-checker', 'Main\SymptomCheckerController@index');

// contact us form -> form submission inserted into db table 'contacts' & emailed to sysadmin (production)
Route::get('/contact-us', 'Main\ContactUsController@index');
Route::post('/contact-us', 'Main\ContactUsController@store');

Route::get('/mailtest', 'Main\ContactUsController@mailtest');

Route::get('/sex-never-gets-old', 'Main\SNGOController@index');

// professionals sample request form -> form submission inserted into db table 'samples'
Route::get('/professionals', 'Main\ProfessionalsController@index');
Route::post('/professionals', 'Main\ProfessionalsController@store');
// leaflet 'download' button beside sample form
Route::get('/professionals/download/{asset}', 'Main\ProfessionalsController@download');

Route::get('/healthcare-professionals', function() {
	return redirect('/professionals');
});

Route::get('/gdpr', function () {
    $exitCode = Artisan::call('gdpr:check', []);
    echo $exitCode;
});

Route::get('sitemap', 'Main\MiscController@sitemap');

// private/admin
Auth::routes();
// login
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');

// logut
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// -----------------------------------
// disable guest registration
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// route /register to login controller
Route::get('register', 'Auth\LoginController@showLoginForm')->name('register');

// -----------------------------------

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['prefix' => 'admin'], function() {
	Route::get('/', function() {
		return redirect('admin/summary');
	});

	Route::get('summary', 'Admin\Summary\SummaryController@index');
	Route::get('summary/samples', 'Admin\Summary\SummaryController@samples_csv');

	// changeed from model binding
	// Route::get('users', 'Admin\Users\UserController@index');
	Route::get('users', [ 'as' => 'admin.users.index', 'uses' => 'Admin\Users\UserController@index']);
	Route::get('users/create', [ 'as' => 'admin.users.create', 'uses' => 'Admin\Users\UserController@create']);
	Route::post('users/store', [ 'as' => 'admin.users.store', 'uses' => 'Admin\Users\UserController@store']);
	Route::get('users/edit/{id}', [ 'as' => 'admin.users.edit', 'uses' => 'Admin\Users\UserController@edit']);
	Route::put('users/update/{id}', [ 'as' => 'admin.users.update', 'uses' => 'Admin\Users\UserController@update']);
	Route::delete('users/destroy/{id}', [ 'as' => 'admin.users.destroy', 'uses' => 'Admin\Users\UserController@destroy']);

});

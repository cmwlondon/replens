define(["jquery", "foundation", "common", "undefined" == typeof pageViewJS ? "undefined" : pageViewJS, "foundation-alert", "foundation-topbar", "foundation-dropdown", "foundation-accordion"], function (e, n, t, i) {
    var o = function () {
        e(document)
            .ready(function () {
                "undefined" != typeof i && (window.pageView = new i());
            })
            .foundation({
                reveal: { animation: "fade", animation_speed: 250, close_on_background_click: !1 },
                orbit: { timer: !1 },
                equalizer: { equalize_on_stack: !0 },
                accordion: {
                    callback: function (e) {
                        console.log(e);
                    },
                },
            });
    };
    return { initialize: o };
});

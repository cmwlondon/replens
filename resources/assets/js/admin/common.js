function trace(e, n) {
    window.console ? console.log(e) : "undefined" != typeof jsTrace && jsTrace.send(e), n && $("#st-debug").html(e);
}
function TrackEventGA(e, n, t, i) {
    "use strict";
    app_live ? ("undefined" != typeof _gaq ? _gaq.push(["_trackEvent", e, n, t, i]) : "undefined" != typeof ga && ga("send", "event", e, n, t, i)) : trace("GA: " + e + "," + n + "," + t + "," + i);
}
function scrollToAnchor(e) {
    var n = $("a[name='" + e + "']");
    $("html,body").animate({ scrollTop: n.offset().top }, "slow");
}
function addSmoothScroll() {
    $("a[href*=#]:not([href=#])").click(function () {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            if (((e = e.length ? e : $("[name=" + this.hash.slice(1) + "]")), e.length)) {
                var n = e.offset().top;
                return $("html,body").animate({ scrollTop: n }, 500), (window.location.hash = this.hash), !1;
            }
        }
    });
}
var app_version = new Date().getTime(),
    app_live = !0,
    slug = function (e) {
        var n = "",
            t = $.trim(e);
        return (
            (n = t
                .replace(/[^a-z0-9-]/gi, "-")
                .replace(/-+/g, "-")
                .replace(/^-|-$/g, "")),
            n.toLowerCase()
        );
    };

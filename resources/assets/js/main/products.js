"use strict";
define(["jquery", "headerHandler"], function(e, n) {
    function t() {
        this.initialize()
    }
    return t.prototype = {
        initialize: function() {
            var t = this;
            new n({
                titleBottom: "0px"
            });
            var i;
            e(window).on("resize", function(e) {
                clearTimeout(i),
                i = setTimeout(function() {
                    t.fixBoxHeight()
                }, 300)
            })
            // ,this.fixBoxHeight()
        },
        fixBoxHeight: function() {
            if (UnisonObj.getWidth() > 768) {
                var n = document.querySelectorAll('div[class*="box-"]')
                  , t = 0;
                for (var i in n) {
                    if ("length" === i || "item" === i)
                        break;
                    t = parseInt(e(n[i]).css("height")) > t ? parseInt(e(n[i]).css("height")) : t
                }
                e(".step").css("height", t)
            } else
                e(".step").css("height", "auto")
        }
    },
    t
});

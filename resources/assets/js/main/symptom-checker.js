'use strict';

/**
 * sympton-checker.js
 * Uses ecmascript 6
 */
define(['jquery', 'vuejs', 'headerHandler', 'localStorage', 'preload'], function ($, Vue, HeaderHandler, LocalStorage) {

	function Me() {
		this.initialize();
	}

	Me.prototype = {
		initialize: function initialize() {

			Vue.config.devtools = true;

			new HeaderHandler();
			var _storage = new LocalStorage();
			var _event = new Vue({});

			$(['moisturiser-packshot.png']).preload('/images/symptoms/');

			/**
    * Result Component
    */
			Vue.component('symptom-result', {
				template: '#template-result',
				props: [],
				data: function data() {
					return {
						show1: false,
						show2: false,
						show3: false,
						showTabOne: true,
						showTabTwo: false,
						showTabThree: false
					};
				},
				mounted: function mounted() {
					_event.$on('toggleResult', this.toggleResult);
				},

				methods: {
					toggleResult: function toggleResult(data) {
						var result = data.result;
						switch (true) {
							case result === 'one':
								this.show1 = true;
								this.show2 = false;
								this.show3 = false;
								break;
							case result === 'two':
								this.show1 = false;
								this.show2 = true;
								this.show3 = false;
								break;
							case result === 'three':
								this.show1 = false;
								this.show2 = false;
								this.show3 = true;
								break;
						}
					},
					toggleTab: function toggleTab(event) {

						var tabEleGroup = document.querySelectorAll('.tab-inner');
						var result = event.target.getAttribute('data-tab');

						for (var i = 0; i < tabEleGroup.length; i++) {
							tabEleGroup[i].classList.remove('highlight');
						}event.target.classList.add('highlight');

						switch (true) {
							case result === 'tabOne':
								this.showTabOne = true;
								this.showTabTwo = false;
								this.showTabThree = false;
								break;
							case result === 'tabTwo':
								this.showTabOne = false;
								this.showTabTwo = true;
								this.showTabThree = false;
								break;
							case result === 'tabThree':
								this.showTabOne = false;
								this.showTabTwo = false;
								this.showTabThree = true;
								break;
						}
					}
				},
				computed: {}
			});

			/**
    * Answer Component
    */
			Vue.component('symptom-answer', {
				template: '#template-answers',
				props: ['value', 'questionId', 'answerId', 'answerLen'],
				data: function data() {
					return {
						id: '',
						selectedId: '0',
						selectClass: ''
					};
				},
				mounted: function mounted() {},

				methods: {
					updateCurrentQuestion: function updateCurrentQuestion(event) {
						event.preventDefault();
						var rowId = event.target.parentElement.parentElement.getAttribute('data-row-id');
						var aid = event.target.getAttribute('data-aid');
						var $eleRow = $('[data-row-id=\'' + rowId + '\']');
						var $eleRowKids = $eleRow.find('a').removeClass('selected');

						$('[data-row-id=\'' + rowId + '\']').find('a[data-aid=\'' + aid + '\']').addClass('selected');

						_event.$emit('updateCurrentQuestion', { data: event });
					}
				},
				computed: {
					defineGrid: function defineGrid() {

						return {
							'columns': true,
							'sm-up-12 lg-up-3': this.answerLen === 4,
							'sm-up-12 lg-up-4': this.answerLen === 3,
							'sm-up-12 lg-up-6': this.answerLen === 2
						};
					}
				},
				filters: {
					filterText: function filterText(val) {
						return val.split('|')[1];
					},
					filterNumber: function filterNumber(val) {
						return val.split('|')[0];
					}
				}
			});

			/**
    * Question/Answer Row Component
    */
			Vue.component('symptom-checker', {
				template: '#template-symptoms',
				props: ['question', 'questionId', 'questionCurrent', 'answerArray'],
				data: function data() {
					return {
						id: 0,
						answerLen: 0,
						answerArr: []
					};
				},
				mounted: function mounted() {
					this.answerLen = this.answerArray.length;
					this.answerArr = this.answerArray;
					this.id = this.$el.getAttribute('data-id');
				},

				methods: {},
				computed: {
					isActive: function isActive() {
						if (this.id <= this.questionCurrent) return true;
					}
				}
			});

			/**
    * Symptom App Wrapper
    */
			var symptomApp = new Vue({
				el: '#symptomApp',
				data: {
					currentQuestion: 0,
					maxQuestion: 8,
					firstRun: true,
					questionsArray: [{
						question: '…irritation?',
						answerArray: ['0|Yes, it&rsquo;s quite severe', '0|Yes, some slight irritation', '2|None at all']
					}, {
						question: '…itching?',
						answerArray: ['0|Yes, it&rsquo;s quite severe', '0|Yes, some slight itching', '2|None at all']
					}, {
						question: '…burning?',
						answerArray: ['0|Yes, it&rsquo;s quite severe', '0|Yes, some slight burning', '2|None at all']
					}, {
						question: '…soreness?',
						answerArray: ['0|Yes, it&rsquo;s quite severe', '0|Yes, some slight soreness', '2|None at all']
					}, {
						question: 'Do you find sex difficult and painful due to dryness?',
						answerArray: ['0|Yes, always', '0|Sometimes', '2|Never']
					}, {
						question: 'Have you experienced any odour down there?',
						answerArray: ['1|Yes, quite often', '1|Yes, sometimes', '2|None at all']
					}, {
						question: 'Do you have any vaginal discharge?',
						answerArray: ['1|Yes, it’s thick, white and curd-like?', '1|Yes, it’s yellow / green and frothy?', '1|Yes, it’s white, grey or thin?', '2|No, nothing at all']
					}, {
						question: 'Are you currently going through the menopause?',
						answerArray: ['0|Yes', '0|I could be', '2|No']
					}, {
						question: 'Are you currently breastfeeding?',
						answerArray: ['0|Yes', '2|No']
					}]
				},
				mounted: function mounted() {
					_event.$on('updateCurrentQuestion', this.updateCurrentQuestion);
				},

				methods: {
					updateCurrentQuestion: function updateCurrentQuestion(event) {
						var qid = event.data.target.getAttribute('data-qid');
						var aid = event.data.target.getAttribute('data-av');
						var val = aid + '|' + event.data.target.text;
						_storage.setVal('answer_' + qid, val);

						if (qid == this.currentQuestion && this.currentQuestion < this.maxQuestion) {
							this.currentQuestion++;
							scrollToAnchor('bottom');
						} else {
							// if(this.firstRun){
							scrollToAnchor('end');
							this.firstRun = false;
							// }
							this.displayResult();
							// clearTimeout(delayedScroll);
							// let delayedScroll = setTimeout(()=>{
							// 	scrollToAnchor('bottom');
							// }, 3 * 1000);
						}
					},
					displayResult: function displayResult() {
						var resultObj = _storage.getAll();
						var resultArr = { one: 0, two: 0, three: 0 };
						var result = "undefined";
						var response3Count = 0;
						var j = 0;

						console.log(resultObj);

						for (var i in resultObj) {
							if (resultObj.hasOwnProperty(i)) {
								var tempText = resultObj[i];
								var tempArr = tempText.split('|');
								var tempVal = parseInt(tempArr[0]);
								switch (true) {
									case tempVal === 0:
										resultArr.one++;
										break;
									case tempVal === 1:
										console.log('response 2');
										console.log(tempText);
										resultArr.two++;
										break;
									case tempVal === 2:
										if (j < 5) response3Count++;
										resultArr.three++;
										break;
								}

								j++;
							}
						}

						var tempResult = Object.keys(resultArr).reduce(function (a, b) {
							return resultArr[a] > resultArr[b] ? a : b;
						});

						// console.log(resultArr.two);
						if (resultArr.two > 0) // prioritize response 2
							result = 'two';else if (response3Count >= 5) // check one majority
							result = 'three';else if (tempResult === 'one') // check one majority
							result = tempResult;
						//
						// console.log(response3Count)
						// console.log(result);

						if (this.currentQuestion == 8) _event.$emit('toggleResult', { result: result });
					}
				}
			});
		}
	};

	return Me;
});
"use strict";
define(["jquery", "carousel", "videojs"], function(t, e, o) {
    function i() {
        this.player = {},
        this.$videoBtn = t(".video-btn"),
        this.$formBtn = t(".form-btn"),
        this.initialize()
    }
    return i.prototype = {
        initialize: function() {
            var o = this;
            new e({
                container: "div.carousel-inner",
                orbContainer: "ul.carousel-control",
                slideText: "label.slide-description",
                callback: function() {
                    t(".carousel").removeClass("hide")
                }
            });
            // this.fixBoxHeight(),
            this.fixVideoHeight(),
            this.windowResize(function() {
                o.fixVideoHeight()
                // ,o.fixBoxHeight()
            }),
            this.$videoBtn.on("click", function(t) {
                t.preventDefault(),
                scrollToAnchor("video")
            }),
            this.$formBtn.on("click", function(t) {
                t.preventDefault(),
                scrollToAnchor("form")
            })
        },
        fixBoxHeight: function() {
            if (UnisonObj.getWidth() > 768) {
                var e = document.querySelectorAll('div[class*="box-"]')
                  , o = 0;
                for (var i in e) {
                    if ("length" === i || "item" === i)
                        break;
                    o = parseInt(t(e[i]).css("height")) > o ? parseInt(t(e[i]).css("height")) : o
                }
                t(".step").css("height", o)
            } else
                t(".step").css("height", "auto")
        },
        fixVideoHeight: function() {
            var e = t(".video-section .inner").find("iframe").outerWidth()
              , o = Math.ceil(.562 * e);
            t(".video-section .inner").find("iframe").outerHeight(o + "px")
        },
        windowResize: function(e) {
            var o;
            t(window).on("resize", function(t) {
                clearTimeout(o),
                o = setTimeout(function() {
                    "undefined" != typeof e && e()
                }, 300)
            })
        }
    },
    i
});

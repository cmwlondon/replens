"use strict";
define(["jquery", "carousel"], function(e, n) {
    function t() {
        this.initialize()
    }
    return t.prototype = {
        initialize: function() {
            new n({
                container: "div.carousel-inner",
                orbContainer: "ul.carousel-control",
                slideText: "label.slide-description",
                callback: function() {
                    e(".carousel").removeClass("hide")
                }
            })
        }
    },
    t
});

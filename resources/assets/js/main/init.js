"use strict";
define(["jquery", "common", "unison-model", "tweenlite", "cssPlugin", "scrolltoPlugin", "" == pageViewJS ? NULL : pageViewJS], function(e, i, o, t, n, s, a) {
    function c() {
        this.initialize()
    }
    return c.prototype = {
        cookieDisclaimer: !1,
        cookieName: "ReplensDisclaimerCookie",
        initialize: function() {
            var i = this;
            "fixed" != e("#noIE").css("position") && (window.UnisonModel = new o,
            trace("JS is good"),
            window.UnisonObj = new o,
            new a,
            this.initMobileMenu(),
            e(".cookie-disclaimer .btn-close").bind("click", function() {
                i.closeCookieDisclaimer()
            }),
            this.cookieDisclaimer = !this.checkCookie(),
            this.updateDisclaimerStyles(),
            window.clearCookie = function() {
                i.clearCookie()
            }
            )
        },
        initMobileMenu: function() {
            t.to(e("#mobile-white-out"), 0, {
                opacity: 0
            }),
            e(".burger").bind("click", {
                parent: this
            }, function(e) {
                "open" == e.data.parent.mobileMenu ? e.data.parent.toggleMenu("close") : e.data.parent.toggleMenu("open")
            }),
            e("#mobile-white-out").bind("click", {
                parent: this
            }, function(e) {
                "open" == e.data.parent.mobileMenu && e.data.parent.toggleMenu("close")
            }),
            e(".slide-menu .close").bind("click", {
                parent: this
            }, function(e) {
                "open" == e.data.parent.mobileMenu && e.data.parent.toggleMenu("close")
            }),
            window.addEventListener("unichange", this.bpChange.bind(this), !1)
        },
        toggleMenu: function(i) {
            this.mobileMenu = i,
            "open" == i ? (t.to(e("#wrapper"), .3, {
                css: {
                    marginLeft: "-320px"
                },
                ease: Quad.easeOut
            }),
            t.to(e(".slide-menu"), .3, {
                css: {
                    right: "15px"
                },
                ease: Quad.easeOut
            }),
            t.to(e(".slide-menu-border"), .3, {
                css: {
                    right: "304px"
                },
                ease: Quad.easeOut
            }),
            t.to(e("#mobile-white-out"), .3, {
                opacity: .75,
                onStart: function() {
                    e("#mobile-white-out").removeClass("hidden")
                }
            })) : (t.to(e("#wrapper"), .3, {
                css: {
                    marginLeft: "0"
                },
                ease: Quad.easeOut
            }),
            t.to(e(".slide-menu"), .3, {
                css: {
                    right: "-290px"
                },
                ease: Quad.easeOut
            }),
            t.to(e(".slide-menu-border"), .3, {
                css: {
                    right: "284px"
                },
                ease: Quad.easeOut
            }),
            t.to(e("#mobile-white-out"), .3, {
                opacity: 0,
                onComplete: function() {
                    e("#mobile-white-out").addClass("hidden")
                }
            }))
        },
        bpChange: function() {
            window.UnisonModel.getWidth() >= 769 && "open" == this.mobileMenu && this.toggleMenu("close")
        },
        updateDisclaimerStyles: function() {
            this.cookieDisclaimer ? e(".cookie-disclaimer").addClass("shown") : e(".cookie-disclaimer").removeClass("shown")
        },
        closeCookieDisclaimer: function() {
            this.cookieDisclaimer = !1,
            this.updateDisclaimerStyles(),
            this.setCookie()
        },
        setCookie: function() {
            var e = new Date;
            e.setTime(e.getTime() + 36e5);
            var i = "expires=" + e.toGMTString();
            document.cookie = this.cookieName + "=policy;" + i + ";path=/"
        },
        getCookie: function() {
            for (var e = this.cookieName + "=", i = decodeURIComponent(document.cookie).split(";"), o = 0; o < i.length; o++) {
                for (var t = i[o]; " " == t.charAt(0); )
                    t = t.substring(1);
                if (0 == t.indexOf(e))
                    return t.substring(e.length, t.length)
            }
            return ""
        },
        checkCookie: function() {
            return "" != this.getCookie()
        },
        clearCookie: function() {
            var e = "expires=" + (new Date).toGMTString();
            document.cookie = this.cookieName + "=policy;" + e + ";path=/",
            this.cookieDisclaimer = !0,
            this.updateDisclaimerStyles()
        }
    },
    c
});

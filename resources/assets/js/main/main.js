require.config({
    baseUrl: "./js/",
    urlArgs: "bust=" + (new Date).getTime(),
    waitSeconds: 0,
    paths: {
        jquery: "libs/jquery/jquery-2.1.0.min",
        json2: "libs/json2/json2-min",
        vuejs: "libs/vuejs/vue",
        preload: "libs/preload/image-preload.min",
        tweenlite: "libs/gsap/TweenLite.min",
        cssPlugin: "libs/gsap/plugins/CSSPlugin.min",
        scrolltoPlugin: "libs/gsap/plugins/ScrollToPlugin.min",
        unison: "libs/unison/unison.min",
        hammer: "libs/hammer/hammer.min",
        fancybox: "libs/fancybox/jquery.fancybox.pack",
        "fancybox-media": "libs/fancybox/helpers/jquery.fancybox-media",
        videojs: "//vjs.zencdn.net/5.4.6/video.min",
        common: "main/partials/common.min",
        "unison-model": "main/partials/unison-model.min",
        headerHandler: "main/partials/header-handler.min",
        carousel: "main/partials/carousel.min",
        openCloseAction: "main/partials/open-close-action.min",
        localStorage: "main/partials/local-storage.min",
        init: "main/init.min"
    },
    shim: {
        vuejs: {
            exports: "Vue"
        },
        unison: {
            exports: "Unison"
        },
        fancybox: {
            deps: ["jquery"],
            exports: "FancyBox"
        },
        "fancybox-media": {
            deps: ["jquery", "fancybox"],
            exports: "FancyBoxMedia"
        },
        tweenlite: {
            exports: "TweenLite"
        },
        cssPlugin: {
            deps: ["tweenlite"],
            exports: "CSSPlugin"
        },
        scrolltoPlugin: {
            deps: ["tweenlite"],
            exports: "ScrollToPlugin"
        },
        videojs: {
            deps: [],
            exports: "videojs"
        }
    }
}),
require(["init"], function(e) {
    window._app = new e
});

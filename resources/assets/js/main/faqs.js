"use strict";
define(["jquery", "headerHandler", "openCloseAction"], function(e, n, t) {
    function i() {
        this.initialize()
    }
    return i.prototype = {
        initialize: function() {
            new n,
            new t
        }
    },
    i
});

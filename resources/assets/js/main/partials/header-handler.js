"use strict";
define(["jquery"], function(e) {
    function n(e) {
        this.titleBottom = parseInt("undefined" != typeof e ? e.titleBottom : 0),
        this.initialize()
    }
    return n.prototype = {
        initialize: function() {
            var n = this;
            this.$headerEle = e("#menuWrapper"),
            this.$bannerEle = e(".header-banner");
            var t;
            e(window).resize(function() {
                clearTimeout(t),
                t = setTimeout(function() {
                    n.fixBannerHeight()
                }, 300)
            }),
            this.fixBannerHeight()
        },
        fixBannerHeight: function() {
            if (UnisonObj.getWidth() < 1440) {
                if (UnisonObj.getWidth() < 961)
                    var n = this.$bannerEle.outerHeight() + (parseInt(this.$bannerEle.css("marginTop")) + this.titleBottom) - 10;
                else
                    var n = this.$bannerEle.outerHeight() + parseInt(this.$bannerEle.css("marginTop")) - 10;
                e(this.$headerEle).css("height", n)
            } else
                e(this.$headerEle).css("height", .33 * this.$headerEle.outerWidth())
        }
    },
    n
});

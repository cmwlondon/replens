function trace(e, i) {
    window.console ? console.log(e) : "undefined" != typeof jsTrace && jsTrace.send(e),
    i && $("#st-debug").html(e)
}
function TrackEventGA(e, i, t, n) {
    "use strict";
    app_live ? "undefined" != typeof _gaq ? _gaq.push(["_trackEvent", e, i, t, n]) : "undefined" != typeof ga && ga("send", "event", e, i, t, n) : trace("GA: " + e + "," + i + "," + t + "," + n)
}
function scrollToAnchor(e) {
    var i = $("a[name='" + e + "']");
    $("html,body").animate({
        scrollTop: i.offset().top
    }, "swing")
}
function addSmoothScroll() {
    $("a[href*=#]:not([href=#])").click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
            var e = $(this.hash);
            if (e = e.length ? e : $("[name=" + this.hash.slice(1) + "]"),
            e.length) {
                var i = e.offset().top;
                return $("html,body").animate({
                    scrollTop: i
                }, 500),
                window.location.hash = this.hash,
                !1
            }
        }
    })
}
function refreshStyles() {
    $.each($("link[toRefresh]"), function(e) {
        if (void 0 != $(this).attr("toRefresh")) {
            var i = $(this).attr("toRefresh") + "?" + Math.round(1e5 * Math.random());
            $(this).attr("href", i)
        }
    })
}
var app_version = (new Date).getTime()
  , app_live = !1;
Number.prototype.format = function(e, i) {
    var t = "\\d(?=(\\d{" + (i || 3) + "})+" + (e > 0 ? "\\." : "$") + ")";
    return this.toFixed(Math.max(0, ~~e)).replace(new RegExp(t,"g"), "$&,")
}
,
"function" != typeof Object.assign && !function() {
    Object.assign = function(e) {
        "use strict";
        if (void 0 === e || null === e)
            throw new TypeError("Cannot convert undefined or null to object");
        for (var i = Object(e), t = 1; t < arguments.length; t++) {
            var n = arguments[t];
            if (void 0 !== n && null !== n)
                for (var o in n)
                    n.hasOwnProperty(o) && (i[o] = n[o])
        }
        return i
    }
}();

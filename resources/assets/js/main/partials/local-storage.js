"use strict";
define([], function () {
    function i() {
        this.initialize();
    }
    return (
        (i.prototype = {
            initialize: function () {
                console.log("local storage");
            },
            setVal: function (i, e) {
                localStorage.setItem(i, e);
            },
            getVal: function (i) {
                return localStorage.getItem(i);
            },
            getKey: function (i) {
                return localStorage.key(i);
            },
            getAll: function () {
                return localStorage;
            },
            remove: function (i) {
                localStorage.removeItem(i);
            },
            clearAll: function () {
                localStorage.clear();
            },
        }),
        i
    );
});

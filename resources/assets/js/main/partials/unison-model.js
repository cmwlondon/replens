"use strict";
define(["jquery", "unison"], function (e, n) {
    var t = function () {
        (this._bp = null), (this.isMobile = !1), (this.isIE = !1), (this.ltIe10 = !1), this.constructor();
    };
    return (
        (t.prototype = {
            constructor: function () {
                var e = document.createEvent("Event");
                e.initEvent("DOMContentLoaded", !0, !0), window.document.dispatchEvent(e);
                var t = document.createEvent("Event");
                t.initEvent("unichange", !0, !0), (this._bp = n.fetch.now());
                var i = this;
                n.on("change", function (e) {
                    (i._bp = e), window.document.dispatchEvent(t), i.fixMenus();
                }),
                    /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && (this.isMobile = !0),
                    /rv:11.0|MSIE 10|MSIE 9/i.test(navigator.userAgent) && (this.isIE = !0),
                    /MSIE 9/i.test(navigator.userAgent) && (this.ltIe10 = !0);
            },
            fixMenus: function () {},
            getWidth: function () {
                return parseInt(this._bp.width);
            },
        }),
        t
    );
});

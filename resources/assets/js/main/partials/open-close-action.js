"use strict";
define(["jquery"], function(e) {
    function n() {
        this.$circle = e(".circle"),
        this.$poly2 = e(".poly-2"),
        this.$poly3 = e(".poly-3"),
        this.$more = e(".more-info"),
        this.clicked = !0,
        this.initialize()
    }
    return n.prototype = {
        initialize: function() {
            this.$circle.on("mouseover", function() {
                this.clicked && e(this).find(".poly-3").addClass("rotate")
            }),
            this.$circle.on("mouseout", function() {
                this.clicked && e(this).find(".poly-3").removeClass("rotate")
            }),
            this.$circle.on("click", function() {
                e(this).find(".poly-2").toggleClass("rotate"),
                e(this).parent().parent().next().find(".more-info").toggleClass("reveal"),
                this.clicked ? (e(this).find(".poly-3").removeClass("rotate"),
                this.clicked = !1) : this.clicked = !0
            })
        }
    },
    n
});

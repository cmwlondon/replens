"use strict";
function _classCallCheck(e, t) {
    if (!(e instanceof t))
        throw new TypeError("Cannot call a class as a function")
}
var _createClass = function() {
    function e(e, t) {
        for (var n = 0; n < t.length; n++) {
            var i = t[n];
            i.enumerable = i.enumerable || !1,
            i.configurable = !0,
            "value"in i && (i.writable = !0),
            Object.defineProperty(e, i.key, i)
        }
    }
    return function(t, n, i) {
        return n && e(t.prototype, n),
        i && e(t, i),
        t
    }
}();
define(["jquery", "hammer", "common"], function(e, t) {
    var n = function() {
        return window[t.prefixed(window, "requestAnimationFrame")] || function(e) {
            setTimeout(e, 1e3 / 60)
        }
    }()
      , i = function(e) {
        return !!(e & t.DIRECTION_HORIZONTAL)
    };
    return function() {
        function r(n) {
            var s = this;
            _classCallCheck(this, r),
            Object.assign(this, n),
            this.container = document.querySelector(this.container),
            this.orbContainer = document.querySelector(this.orbContainer),
            this.panes = Array.prototype.slice.call(this.container.children, 0),
            this.direction = t.DIRECTION_HORIZONTAL,
            this.containerSize = i(this.direction) ? this.container.offsetWidth : 0,
            this.currentIndex = 0,
            this.scrollThreshold = 30,
            this.cycleSeconds = 10,
            this.cycle = null,
            this.imageMapFired = !1,
            this.noTextRefresh = !0,
            this.CarouselDone = document.createEvent("Event"),
            this.CarouselDone.initEvent("carouselDone", !0, !0);
            var a;
            e(window).resize(function() {
                clearInterval(a),
                a = setTimeout(function() {
                    s.containerSize = s.container.offsetWidth,
                    s.noTextRefresh = !0,
                    s.initialiseHammer()
                }, 300)
            }),
            this.initialiseHammer()
        }
        return _createClass(r, [{
            key: "initialiseHammer",
            value: function() {
                var t = this;
                e("." + this.orbContainer.className + " > li > a").off(),
                e("." + this.orbContainer.className + " > li > a").on("click", function(n) {
                    n.preventDefault();
                    var i = e(n.target).context.parentElement.getAttribute("data-id");
                    t.hightlightOrb(i),
                    t.show(i, 0, !0),
                    t.cycleNext()
                }),
                clearInterval(this.cycle),
                this.show(this.currentIndex),
                this.callback(),
                this.noTextRefresh = !1
            }
        }, {
            key: "show",
            value: function(e, t, n) {
                e = Math.max(0, Math.min(e, this.panes.length - 1)),
                t = t || 0;
                var i = this.container.className;
                n ? -1 === i.indexOf("animate") && (this.container.className += " animate") : -1 !== i.indexOf("animate") && (this.container.className = this.container.className.replace("animate", "").trim());
                var r, s;
                for (r = 0; r < this.panes.length; r++)
                    s = "translate(" + this.containerSize / 100 * (100 * (r - e) + t) + "px, 0)",
                    this.panes[r].style.transform = s,
                    this.panes[r].style.mozTransform = s,
                    this.panes[r].style.webkitTransform = s,
                    this.panes[r].style.msTransform = s;
                this.currentIndex = e,
                this.noTextRefresh || this.swapText()
            }
        }, {
            key: "onPan",
            value: function(e) {
                var t = i(this.direction) ? e.deltaX : 0
                  , r = 100 / this.containerSize * t
                  , s = !1;
                "panend" != e.type && "pancancel" != e.type || (Math.abs(r) > this.scrollThreshold && "panend" == e.type && (this.currentIndex += r < 0 ? 1 : -1,
                this.hightlightOrb(this.currentIndex),
                this.cycleNext()),
                r = 0,
                s = !0),
                n(this.show.bind(this, this.currentIndex, r, s))
            }
        }, {
            key: "swapText",
            value: function() {
                var t = e(this.slideText)
                  , n = e(this.panes[this.currentIndex]).find("div.hidden").html();
                t.addClass("hide");
                setTimeout(function() {
                    t.html(n),
                    t.removeClass("hide")
                }, 300)
            }
        }, {
            key: "hightlightOrb",
            value: function(t) {
                e("." + this.orbContainer.className + " > li").removeClass("active"),
                e("." + this.orbContainer.className + " > li").each(function(e, n) {
                    -1 === t && (t = 0),
                    parseInt(n.getAttribute("data-id")) === parseInt(t) && (n.className = "active")
                })
            }
        }, {
            key: "cycleNext",
            value: function() {
                var e = this;
                clearInterval(this.cycle),
                this.cycle = setInterval(function() {
                    e.currentIndex >= e.panes.length - 1 ? e.currentIndex = 0 : e.currentIndex = e.currentIndex + 1;
                    e.hightlightOrb(e.currentIndex),
                    e.show(e.currentIndex, 0, !0)
                }, 1e3 * this.cycleSeconds)
            }
        }, {
            key: "supportsTransitions",
            value: function() {
                var e = (document.body || document.documentElement).style
                  , t = "transition";
                if ("string" == typeof e[t])
                    return !0;
                var n = ["Moz", "webkit", "Webkit", "Khtml", "O", "ms"];
                t = t.charAt(0).toUpperCase() + t.substr(1);
                for (var i = 0; i < n.length; i++)
                    if ("string" == typeof e[n[i] + t])
                        return !0;
                return !1
            }
        }]),
        r
    }()
});

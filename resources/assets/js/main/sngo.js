define(["jquery", "carousel", "videojs"], (function(i, e, t) {
    function o() {
        this.player = {
        },
        this.$videoBtn = i(".video-btn"),
        this.$formBtn = i(".form-btn"),
        this.initialize()
    }
    return o.prototype = {
        initialize() {
            new e({
                container: "div.carousel-inner",
                orbContainer: "ul.carousel-control",
                slideText: "label.slide-description",
                callback: function() {
                    i(".carousel").removeClass("hide")
                }
            });
            // this.fixBoxHeight(),
            // this.fixVideoHeight(),
            this.windowResize((()=>{
                this.fixVideoHeight(),
                this.fixBoxHeight()
            }
            )),
            this.$videoBtn.on("click", (i=>{
                i.preventDefault(),
                scrollToAnchor("video")
            }
            )),
            this.$formBtn.on("click", (i=>{
                i.preventDefault(),
                scrollToAnchor("form")
            }
            ))
        },
        fixBoxHeight() {/*	
if(UnisonObj.getWidth()>768){
let t=document.querySelectorAll('div[class*="box-"]');
var e=0;
for(let o in t){
if("length"===o||"item"===o)break;
e=parseInt(i(t[o]).css("height"))>e?parseInt(i(t[o]).css("height")):e
}i(".step").css("height",e)
}else i(".step").css("height","auto")
*/
        },
        fixVideoHeight() {/*
	var e=i(".video-section .inner").find("iframe").outerWidth(),t=Math.ceil(.562*e);
	i(".video-section .inner").find("iframe").outerHeight(`${t}px`)
	*/
        },
        windowResize(e) {
            var t;
            i(window).on("resize", (i=>{
                clearTimeout(t),
                t = setTimeout((()=>{
                    void 0 !== e && e()
                }
                ), 300)
            }
            ))
        }
    },
    o
}
));

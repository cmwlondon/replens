@extends('admin.layouts.main')

@section('content')
<div class="row pt2">
	<div class="small-12 columns">
		<div class="row">
			<div class="panel medium-6 medium-offset-3 small-10 small-offset-1 columns">
				<h2>Login</h2>

				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<strong>Whoops!</strong> There were some problems with your input.<br><br>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{!! $error !!}</li>
							@endforeach
						</ul>
					</div>
				@endif
				
				{!! Form::open(['url'=>'login', 'class'=>'form-horizontal']) !!}

					{!! Form::hidden('_token', csrf_token()) !!}

					{!! Form::label('email', 'E-Mail Address') !!}
					{!! Form::text('email') !!}
					{!! $errors->first('email', '<small class="error">:message</small>') !!}
			
					{!! Form::label('password', 'Password') !!}
					{!! Form::password('password') !!}
					{!! $errors->first('password', '<small class="error">:message</small>') !!}

					<div class="checkbox">
						{!! Form::checkbox('remember') !!}
						{!! Form::label('remember', 'Remember Me') !!}
					</div>
					
					@if (isset($redirect) && $redirect != '')
						{!! Form::hidden('redirectTo', $redirect) !!}
					@endif

					{!! Form::submit('Login', array('class' => 'button success small', 'style' => 'margin-right:15px;')) !!}
					{!! Html::link('/admin/password/email', 'Forgot Your Password?') !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

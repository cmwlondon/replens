@extends('admin.layouts.main')

@section('content')

	@include('admin.layouts.partials.header')

	<div class="row">
		<div class="columns small-12 medium-6">
			<h3>Sample Requests</h3>
			<table width="100%">
			  <thead>
			    <tr>
			      <th>Request Type</th>
			      <th width="150">Count</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach ($samples_summary as $row)
			  		<tr>
				      <td>{{{$row->patients}}}</td>
				      <td>{{{$row->count}}}</td>
				    </tr>
			  	@endforeach
			  </tbody>
			</table>
			<p><a href="/admin/summary/samples">Download as CSV</a></p>
		</div>

		
	</div>
@endsection

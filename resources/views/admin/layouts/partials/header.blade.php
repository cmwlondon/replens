{!! Admin::getAuthUser() !!}
<nav class="top-bar mb1" data-topbar role="navigation">
	<ul class="title-area">
		<li class="name">
			<h1>{!! Html::link('/', 'Replens', ['class'=>'']) !!}</h1>
		</li>
		<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
		<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	</ul>

	<section class="top-bar-section">
		<!-- Right Nav Section -->
		<ul class="right">

			<li class="divider"></li>
			<li class="has-dropdown">
				<a href="#">{!! Admin::user('name') !!}</a>
				<ul class="dropdown">
					<li>{!! link_to_route('admin.users.edit', 'My Account', [Admin::user('id')], null) !!}</li>
					<li class="divider"></li>
					<li class="alert">{!! link_to('logout', 'Logout', [], null) !!}</li>
				</ul>
			</li>
		</ul>

		<!-- Left Nav Section -->
		<ul class="left">
			<li>{!! Html::link('/admin/summary', 'Summary', ['class'=>'']) !!}</li>
			
			@if (Admin::checkPermission('menu_users'))
			<li class="has-dropdown not-click">{!! Html::link('#', 'Users', ['class'=>'']) !!}
				<!-- Dropdown -->
				<ul class="dropdown">
					<li>{!! Html::link('/admin/users', 'List Users', ['class'=>'']) !!}</li>
					@if (Admin::checkPermission('create_user'))
					<li>{!! Html::link('/admin/users/create', 'Create User', ['class'=>'']) !!}</li>
					@endif
				</ul>
			</li>
			@endif

			
		</ul>
	</section>
</nav>

@include('admin.layouts.partials.responses')
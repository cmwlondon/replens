@extends('main.layouts.main')


@section('header')

@endsection


@section('content')
<div class="row full contact-content">

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-8 before-2 after-2 sm-10 sm-before-1 sm-after-1">
					<h1>CHURCH &amp; DWIGHT WEBSITE TERMS AND CONDITIONS</h1>
					<p></p>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">
			<div class="row full">
				<div class="columns span-8 before-2 after-2 sm-10 sm-before-1 sm-after-1">
					<h3>Operation of the Site</h3>
					<p>
						This website (&ldquo;Site&rdquo;) is operated by Church &amp; Dwight UK Limited (&ldquo;Church &amp; Dwight&rdquo;), a limited company registered in England and Wales under company number 00375793 with its registered office at Wear Bay Road, Folkestone, Kent, CT19 6PG.
						Your use of the Site
						These terms and conditions (&ldquo;Terms&rdquo;) govern your use of the Site. By using the Site you agree to be bound by these Terms. If you do not agree with any part of these Terms, please refrain from using the Site.
						In using the Site you may not: (i) interfere with or disrupt any network or website connected to this Site or gain unauthorised access to any computer systems; (ii) interfere with the enjoyment or use of the Site by any other person; or (iii) use this Site or the content on this Site for any purpose which is unlawful.
					</p>

					<h3>Intellectual property</h3>
					<p>
						Church &amp; Dwight is the owner or the licensee of all rights, title and interest to this Site and its content, including all designs, text, graphics, photographs, illustrations, images, graphic materials, button icons, data compilations and software used in connection with this Site.
						You may not report, modify, publish, sell, reproduce, distribute, post, display, transmit, or in any way exploit any of this Site's contents for commercial purposes without obtaining a licence to do so from Church &amp; Dwight or its licensors. You may, if you wish, download and retain on a disk or in hard drive form a single copy, and may download extracts, of any page(s) from the Site for personal, non-commercial purposes. You must not modify the paper or digital copies of any materials you have printed off or downloaded in any way, and you must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text. Church &amp; Dwight's status (and that of any identified contributors) as the authors of the content on the Site must always be acknowledged. If you print off, copy or download any part of the Site in breach of these terms of use, your right to use the Site will cease immediately and you must, at Church &amp; Dwight's option, return or destroy any copies of the materials you have made.
						The trade marks, logos and service marks (&ldquo;Marks&rdquo;) displayed throughout the Site are registered and unregistered Marks of Church &amp; Dwight and/or third party licensors. No license, right or permission is granted to you for any use of the Marks by you or anyone authorised by you. You may not use any meta tags or any other &ldquo;hidden text&rdquo; which uses any Marks. Misuse of the Marks is strictly prohibited and Church &amp; Dwight will aggressively enforce its intellectual property rights to the fullest extent of the law.
						Reliance on the contents of the Site
						While Church &amp; Dwight has made reasonable efforts to include information at this Site which is accurate and timely, Church &amp; Dwight makes no representations or warranties as to the accuracy of such information and, specifically, Church &amp; Dwight assumes no liability or responsibility for any errors or omissions in the information or the contents of this Site. Moreover, Church &amp; Dwight neither warrants nor represents that your use of the information will not infringe the rights of third parties who are not affiliated with Church &amp; Dwight.
						Descriptions of Church &amp; Dwight's products contained within the Site shall not constitute product labelling. You should use Church &amp; Dwight's products in accordance with the instructions contained on the cartons and labels found on those products in the country of purchase.
					</p>

					<h3>Liability</h3>
					<p>
						Whilst Church &amp; Dwight make every effort to ensure the Site is available and secure, given the nature of the Internet, it does not warrant or guarantee this will always be the case. The contents of this Site are provided to you on an &ldquo;as is&rdquo; basis and your access to and use of this Site are at your own risk.
						To the extent permitted by law, neither Church &amp; Dwight nor any party involved in the creation, transmittal, or maintenance of this Site shall be liable to you for any direct, indirect, consequential, incidental or punitive loss or damages of any kind allegedly arising out of your access or use of this Site, or your inability to access or use this Site. Nothing in these Terms excludes Church &amp; Dwight's liability which cannot be excluded or limited under applicable law.
						Church &amp; Dwight assumes no responsibility and shall not be liable for any damages to, or any viruses that may infect, your computer equipment resulting from your access to or use of this Site, or the downloading of any Information from this Site.
					</p>

					<h3>Communications and data</h3>
					<p>
						Any communication or material transmitted to this Site by email or other means, shall be treated as non-confidential and non-proprietary. This includes ideas, suggestions, comments, questions and any other information or data. Anything submitted to Church &amp; Dwight can be used, reproduced, transmitted, disclosed or published by Church &amp; Dwight or its affiliates without restriction or compensation, subject toChurch &amp; Dwight's Internet Privacy Policy
					</p>

					<h3>Links from the Site</h3>
					<p>
						Church &amp; Dwight has not reviewed all of the sites which are linked to this Site. As a result, Church &amp; Dwight is not responsible for the content of such linked sites and your linking to such sites is at your own risk.
						Changes to the Site and Terms
						Church &amp; Dwight reserves the right to alter or delete the contents of this Site at any time without notice, including these Terms. It is your responsibility to familiarise yourself with the Terms to ensure that you are aware of any changes. Your use of this Site following the posting of any such changes will constitute your acceptance of the revised Terms. Church &amp; Dwight further reserves the right to discontinue this Site at any time and without notice.
					</p>

					<h3>Invalidity</h3>
					<p>
						If any of these Terms are deemed invalid, void or unenforceable for any reason, they will be severed from the rest of these Terms which shall remain unaffected.
						Governing law
						These Terms shall be governed by and construed in accordance with English law and the English courts shall have jurisdiction to resolve any disputes arising out of these Terms. Any cause of action with respect to this Site or these Terms must be filed in courts of competent jurisdiction in England within one year after the cause of action has accrued.
					</p>

					<h3>Contact</h3>
					<p>
						You may contact Church &amp; Dwight directly if you have any concerns about material which appears on the Site at Wear Bay Road, Folkestone, Kent, CT19 6PG, F.A.O., Attention: Consumer Relations.
						Thank you for visiting this Site.
					</p>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

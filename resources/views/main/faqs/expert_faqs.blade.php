@extends('main.layouts.main')


@section('header')
	@include('main.faqs.partials._banner_header', ['title' => 'Expert'])
@endsection


@section('content')
<div class="row full faqs-content">

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1">
					<p>For millions of women, vaginal dryness is a real problem that affects and interferes with their day-to-day lives. Vaginal dryness is common and can occur at any age. The effects of vaginal dryness vary from woman to woman, ranging from minor discomfort to painful sexual intercourse. Regardless of the level of discomfort, vaginal dryness can impact your daily life.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1 margin-bottom20">

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat?
									</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat?
									</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</p>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

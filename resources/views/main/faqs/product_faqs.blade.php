@extends('main.layouts.main')


@section('header')
	@include('main.faqs.partials._banner_header', ['title' => 'Product'])
@endsection


@section('content')
<div class="row full faqs-content">

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1">
					<p>Do you want to learn more about how Replens MD<sup>TM</sup> can help replenish vaginal moisture? Read below for our answers to frequently asked questions.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1 margin-bottom20">

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>What causes vaginal dryness?</h3>
									<p>Nearly every woman will experience vaginal dryness sometime in her life. It is most often associated with the normal decline or fluctuation of the female hormone oestrogen. This can be triggered by childbirth, breastfeeding or menopause.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>Dryness can also be caused by taking certain medications, exercising intensively or being under stress. It is also common to experience vaginal dryness when douching, using tampons or at the end of the menstrual cycle.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>How does Replens MD<sup>TM</sup> work?</h3>
									<p>Replens MD<sup>TM</sup> works by lining the inside of the vagina, providing moisture, lubrication and comfort. The gel stays in contact with the vaginal walls for several days while the purified water it contains is absorbed.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<img class="how-work-graphic" src="/images/products/how-work-graphic.png" alt="How Replens MD&trade; Work Graphic" />
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>How often should Replens MD<sup>TM</sup> Vaginal Moisturiser be used?</h3>
									<p>For most women, Replens MD<sup>TM</sup> Vaginal Moisturiser should be used every three days for best results.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>However, depending on the severity of your dryness, Replens MD<sup>TM</sup> can be used more or less frequently, as necessary. Replens MD<sup>TM</sup> is safe to use daily.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>When should Replens MD<sup>TM</sup> Vaginal Moisturiser be used?</h3>
									<p>Replens MD<sup>TM</sup> can be used any time of day or night. Replens MD<sup>TM</sup> works best when used on a regular schedule and not just prior to intercourse.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>As Rep lens MD<sup>TM</sup> delivers long-lasting moisture, there is no need to apply it just prior to intercourse. We recommend using Replens MD<sup>TM</sup> at least 2 hours prior to intercourse to allow for the gel to be absorbed.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Will Replens MD<sup>TM</sup> Vaginal Moisturiser make intimacy more enjoyable?</h3>
									<p>One of the most common ways that women discover vaginal dryness is during intimacy. When used regularly, Replens MD<sup>TM</sup> replenishes your natural vaginal moisture, making intimacy more enjoyable.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>Replens' MD<sup>TM</sup> formula delivers longlasting moisture so sexual intercourse can be more spontaneous. Since Replens MD<sup>TM</sup> does not need to be applied immediately before intercourse, it does not interrupt the moment. Instead, Replens MD<sup>TM</sup> provides long-lasting lubrication whenever the moment is right.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Can Replens MD<sup>TM</sup> be used as birth control?</h3>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>No. Replens MD<sup>TM</sup> does not contain spermicide. It is not a contraceptive.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Can you use Replens MD<sup>TM</sup> before getting a pap smear?</h3>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>We recommend stopping the use of Replens MD<sup>TM</sup> four days before getting a pap smear.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Should I use Replens MD<sup>TM</sup> during my period?</h3>
									<p>Replens MD<sup>TM</sup> is safe for use anytime, but some women may experience discharge if used during their period.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>It is best to resume use of Replens MD TM after your flow completely stops.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Is Replens MD<sup>TM</sup> Vaginal Moisturiser compatible with condoms?</h3>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>Yes, Replens MD<sup>TM</sup> is compatible with natural rubber latex, polyisoprene, and polyurethane condoms.</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Does Replens MD<sup>TM</sup> come in more than one package type?</h3>
									<p>Yes, Replens MD<sup>TM</sup> Vaginal Moisturiser is available in:</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>
										  <ul>
												<li>A 35g tube with reusable applicator (which allows up to 12 applications)</li>
												<li>6 single use prefilled applicators (12 applicators available on prescription)</li>										  	
										  </ul>
									</p>
								</div>
							</div>
						</div>
					</div>

					<div class="row full border-bottom">
						<div class="columns span-1 before-1 after-1 sm-1">
							@include('main.layouts.partials._open-close-btn')
						</div>
						<div class="columns span-9">
							<div class="row full">
								<div class="columns span-12 info">
									<h3>Does Replens MD<sup>TM</sup> Vaginal Moisturiser have any side effects?</h3>
									<p>Some women notice a residue or discharge after initial use of Replens MD<sup>TM</sup>. This is caused by the elimination of dead skin cells. Your body naturally sheds dry vaginal tissue that has built up over time. Replens MD<sup>TM</sup> enhances this renewal process and leaves softer, more supple tissue behind.</p>
								</div>
							</div>
							<div class="row full">
								<div class="columns span-12 more-info">
									<p>When used on a regular basis, Replens MD<sup>TM</sup> will help prevent the build up of dead skin cells and the discharge should dissipate, you may wish to wait an extra day or two between applications. While use is recommended every three days, you may wish to increase or decrease the amount of time between Replens MD<sup>TM</sup> applications to maximise moisture or minimise discharge.</p>
									<p>If vaginal irritation occurs, discontinue use. If symptoms persist, contact your doctor. If you experience any other unpleasant effects after using Replens MD<sup>TM</sup> please consult your doctor.</p>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

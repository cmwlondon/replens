<div class="header-banner">
  <img src="/images/faqs/header-banner.jpg" alt="Replens FAQ Page Header Banner" />
  <h1 class="absPos">
    {{ $title }}<br>
    FAQs
  </h1>
</div>

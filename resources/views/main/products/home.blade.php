@extends('main.layouts.main')


@section('header')
		@include('main.products.partials._banner_header')
@endsection


@section('content')
<div class="row full product-content">
	<div class="columns span-12 hidden-xlarge hidden-xxlarge alt-header-text">
		<div class="row">
			<div class="columns span-10 before-1 after-1 align-center">
				<h1 class="hidden-xxlarge hidden-xlarge">Replens MD<sup>TM</sup> is available in 3 sizes – 1, 2 and 4 weeks supply.</h1>
				<a href="http://www.boots.com/webapp/wcs/stores/servlet/SolrSearchLister?storeId=10052&langId=-1&catalogId=11051&stReq=1&searchTerm=replens#container" target="_blank" class="replens-header-btn" type="button" name="button">buy now</a>
			</div>
		</div>
	</div>

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1">
					<p>Replens MD<sup>TM</sup> Vaginal Moisturiser provides immediate alleviation of dryness symptoms and helps replenish vaginal moisture. Replens MD<sup>TM</sup> is a clinically tested long lasting vaginal moisturiser that leaves vaginal tissues hydrated and feeling rejuvenated.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">
			<div class="row full white-background pdbtm-40 border-bottom">
				<div class="columns span-12">
					<h2 class="flower-bullet-text">Lasts up to 3 days <br/><img src="/images/flower.svg" class="flower" alt="Replens MD&trade; Moisturiser Flower Image" /> <br/>Oestrogen free <br/><img src="/images/flower.svg" class="flower" alt="Replens MD&trade; Moisturiser Flower Image" />&nbsp;<br/>Fragrance&nbsp;free</h2>
					<h2 class="compatible-text">Compatible with most condoms</h2>
					<h4>(natural rubber latex, polyisoprene, and polyurethane)</h4>
				</div>
			</div>

			<div class="row full">
				<div class="columns span-12 step-header">
					<a name="how-does-it-work"></a>
					<h2>How does it work?</h2>
				</div>
			</div>

			<div class="row full overflow-hidden">
				<div class="columns span-4 md-4 md-6 sm-12 step box-1 min-height235">
					<h3>Step 1:</h3>
					<p>Replens MD<sup>TM</sup> Vaginal Moisturiser immediately goes to work providing soothing moisture to dry vaginal cells for long-lasting hydration.</p>
				</div>
				<div class="columns span-4 md-4 md-6 sm-12 step box-2 min-height235">
					<h3>Step 2:</h3>
					<p>Replens MD<sup>TM</sup> Vaginal Moisturiser continues to deliver moisture for up to 3 days, helping to replenish moisture to dry vaginal cells by supplementing the body's natural lubrication.</p>
				</div>
				<div class="columns span-4 md-4 md-12 sm-12 step box-3">
					<h3>Step 3:</h3>
					<p>As the cells of the vaginal wall are regenerated, dry cells and Replens MD<sup>TM</sup> product are eliminated from the body naturally. As with dry skin on your face and hands, regular moisturising well help prevent dryness from recurring.</p>
					<img class="how-work-graphic" src="/images/products/how-work-graphic.png" alt="How Replens MD&trade; Work Graphic" />
				</div>
			</div>

			<div class="row full replens-md">
				<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
					<p class="no-margin-top">Replens MD<sup>&trade;</sup> is the clinically tested, number one vaginal moisturiser GP recommended brand*. Apply Replens MD<sup>TM</sup> as needed to supplement your body&rsquo;s natural lubrication to help alleviate the feminine discomfort associated with vaginal dryness. As with dry skin that you experience on your face and hands, regular moisturising treatment with Replens MD<sup>TM</sup> may be necessary to prevent dryness from recurring. </p>
					<p>Replens MD<sup>TM</sup> is available over-the-counter without a prescription, but has long been recommended by healthcare professionals, making it the number one recommended brand that helps to provide long lasting relief for vaginal dryness.</p>
					<p class="small">*GPrX data – from January 2019 to December 2019 – UK coverage</p>
				</div>

				<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 align-right margin-bottom40">
					<img class="full-width" src="/images/products/replens-md-professional.jpg" alt="Replens MD&trade; Professional" />
				</div>

				<div class="columns span-10 before-1 after-1 border-1">
					<div class="row full pad20">
						<div class="columns span-7 md-12 sm-12">
							<div class="row full">
								<div class="columns span-12">
									<h3>Replens MD<sup>TM</sup> is available in:</h3>
									<img class="full-width" src="/images/shared/fade-line.jpg" alt="" />
									<p>A 35g tube with reusable applicator (which allows up to 12 applications)</p>
									<p style="margin-bottom:2em;">6 single use prefilled applicators (12 applicators available on prescription)</p>
									 
									<p>Available in Sainsbury&rsquo;s, Waitrose, Lloyds Pharmacy, most independent pharmacies, Amazon, Superdrug and&nbsp;Boots.</p>
									<div class="flex">

										<div class="fifty">
											<a class="image-link" href="https://www.sainsburys.co.uk/gol-ui/SearchDisplayView?filters[keyword]=Replens&langId=44&storeId=10151&searchType=2&searchTerm=Replens " target="_blank">
												<img class="sainsburys-logo" src="/images/products/retailers/sainsburys.png" alt="Sainsburys" />
											</a>
											<a class="image-link" href="http://www.lloydspharmacy.com/en/replens-md-longer-lasting-vaginal-moisturiser-35g" target="_blank">
												<img class="lloyds-logo" src="/images/products/retailers/lloydspharmacy.png" alt="Lloyds Pharmacy" />
											</a>

											<a class="image-link" href="https://www.superdrug.com/Replens/Replens-Vaginal-Moisturiser-35g/p/3395" target="_blank">
												<img class="superdrug-logo" src="/images/products/retailers/superdrug.png" alt="Superdrug" />
											</a>
										</div>
										<div class="fifty">
											<a class="image-link" href="https://www.waitrose.com/ecom/products/replens-md-moisturiser/797494-724062-724063" target="_blank">
												<img class="sainsburys-logo" src="/images/products/retailers/waitrose.png" alt="Waitrose" />
											</a>
											<a class="image-link" href="https://www.amazon.co.uk/s?k=Replens&ref=nb_sb_noss_2" target="_blank">
												<img class="amazon-logo" src="/images/products/retailers/amazon.png" alt="Anmazon" />
											</a>
											<a class="image-link" href="http://www.boots.com/webapp/wcs/stores/servlet/ProductDisplay?storeId=10052&productId=923464&callingViewName=&langId=-1&catalogId=11051" target="_blank">
												<img class="boots-logo" src="/images/products/retailers/boots.png" alt="Boots" />
											</a>
										</div>
									</div>

									<!-- <p class="small"></p> -->
									<div class="columns span-12 align-center">
										<a href="http://www.boots.com/webapp/wcs/stores/servlet/SolrSearchLister?storeId=10052&langId=-1&catalogId=11051&stReq=1&searchTerm=replens#container" target="_blank" class="replens-md-btn wtb-md-btn hidden-medium hidden-small" type="button" name="button">buy now</a>
									</div>
									</div>
								</div>
							</div>

							<div class="columns span-5  md-12 sm-12 align-center">
								<img class="full-width-80" src="/images/products/products-wtb.png" alt="Vaginal Moisturiser Products - Replens MD&trade;" />
							</div>
							<div class="columns span-12 align-center">
								<a href="http://www.boots.com/webapp/wcs/stores/servlet/SolrSearchLister?storeId=10052&langId=-1&catalogId=11051&stReq=1&searchTerm=replens#container" target="_blank" class="replens-md-btn hidden-large hidden-xlarge hidden-xxlarge" type="button" name="button">buy now</a>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

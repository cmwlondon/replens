@extends('main.layouts.main')

@section('header')
	@include('main.sngo.partials._banner_header')
@endsection

@section('content')
<div class="sngo">
	<div class="content">
		<h2>Replens<sup>&trade;</sup> wins Transport for London (TfL) diversity in advertising competition with Sex Never Gets Old&nbsp;campaign.</h2>

		<figure class="fr"><img alt="desire never gets old" src="/images/sngo/pic1.jpg"></figure>
		<p>The “<strong>Sex Never Gets Old</strong>” campaign features a diverse group of older people, with a range of ethnicities, ages and sexualities, within beautiful, emotional portraits of couples. Through these emotive portraits Replens<sup>&trade;</sup> highlights that many people continue to enjoy sex and intimacy well into their sixties and beyond. The imagery and headlines emphasise a range of experiences and moments, from “<strong>touch never gets old</strong>” to “<strong>sex never gets old</strong>” – this is not just about the sexual act, but the broader canvas of human connection.
		</p>

		
		<p>The campaign is a celebration of Replens<sup>&trade;</sup> inclusive brand values and gives visibility and awareness to the core brand community who Replens<sup>&trade;</sup> looked to hero in this campaign. Research tells us that despite 17.7% of the UK population being 65 or over, they only feature in 6.17% of advertising material (Lloyds Banking research, 2016).  Whilst feedback from the brand community indicated a perceived level of societal discomfort with sex and intimacy in the those aged over 60. ReplensMD<sup>&trade;</sup> felt it was time to change this. Replens<sup>&trade;</sup> has also commissioned a nationwide survey with <strong>One Poll</strong> of people (over the age of 60) to better understand their views on sex and intimacy and identify the factors that contribute to a long-lasting sex life.</p>

		<p><strong>The Flames of Sex &amp; Intimacy Survey</strong> reveals that despite society’s often negative views of sex and intimacy in those over 60, many older people grow in confidence and become more at ease in relationships enjoying connection and sexual satisfaction throughout their 60’s, 70’s and beyond. </p>

		<div class="triple">
			<figure><img alt="desire never gets old" src="/images/sngo/pic2.jpg"></figure>
			<figure><img alt="sex never gets old" src="/images/sngo/pic3.jpg"></figure>
			<figure><img alt="touch never gets old" src="/images/sngo/pic4.jpg"></figure>
		</div>

		<blockquote>“When you start to get older, people wrongly assume that you stop having sex. We’re proud to work with Replens<sup>&trade;</sup> and be challenging the taboo that sexual intimacy amongst us slightly older folk shrinks with age. We’re firm believers that sex never gets old. If anything, it gets better the more experienced you are!”</blockquote>
		<p class="quote">Rose &amp; Linda, Featured couple</p>

	</div>
</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

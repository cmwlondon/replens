<div class="carousel hide">
  <div class="slides carousel-inner homeSlides">

    <div class="slide pane image-99">
      <div class="hidden">A long-lasting intimate moisturiser for a long-lasting sex life.</div>
      <div class="tb">
        <a href="/sex-never-gets-old">Learn more</a>
      </div>
    </div>

    <div class="slide pane image-1 shift-image-6">
      <div class="bar"><img src="/images/home/rekindle.svg" alt="Reignite Flames of Intimacy"/></div>
      <div class="hidden">Reignite the flames of intimacy with Replens MD<sup>TM</sup> Vaginal Moisturiser</div>
    </div>
    <div class="slide pane image-2 shift-image-5">
      <div class="bar"><img src="/images/home/rejuvenate.svg" alt="Rejuvenating Dry Vaginal Cells"/></div>
      <div class="hidden">Replens MD<sup>TM</sup> immediately soothes dry vaginal cells and then helps replenish moisture for up to 3 days of long lasting relief with one application.</div>
    </div>
    <div class="slide pane image-3 shift-image-3">
      <div class="bar"><img src="/images/home/recommended.svg" alt="Recommended brand for vaginal dryness*"/></div>
      <div class="hidden">Replens MD<sup>TM</sup> is the UK&rsquo;s number one GP recommended brand for vaginal dryness*
       <span>*GPrX data – from January 2019 to December 2019 – UK coverage</span>
      </div>
    </div>
    <div class="slide pane image-4 shift-image-6">
      <div class="bar"><img src="/images/home/replens.svg" alt="Vaginal Atrophy &amp; Dryness Relief"/></div>
      <img class="image-4-text hidden-small hidden-medium hidden-large hidden-xlarge" src="/images/home/bye-bye-dry.png" alt="" />
      <img class="image-4-text mobile-opcaity  hidden-xxlarge" src="/images/home/bye-bye-dry-alt.png" alt="" />
      <img class="image-4-packshot alt" src="/images/home/carousel4-vm.png" alt="" />
      <div class="hidden">Wave bye bye to vaginal dryness and say hello to happier days to come.</div>
    </div>
  </div>

  <div class="controls">
    <div class="row">
      <div class="columns span-12 mb0">
        <label class="slide-description">A long-lasting intimate moisturiser for a long-lasting sex life.</label>
        <ul class="carousel-control">
          <li data-id="0" class="active"><a href="#"></a></li>
          <li data-id="1"><a href="#"></a></li>
          <li data-id="2"><a href="#"></a></li>
          <li data-id="3"><a href="#"></a></li>
          <li data-id="4"><a href="#"></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>

{{--
<div class="sngoMainBanner">
    <div class="image-99">
      <div class="tb">
        <a href="/sex-never-gets-old">Learn more</a>
      </div>
    </div>
</div>
--}}

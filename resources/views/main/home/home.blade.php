@extends('main.layouts.main')


@section('header')
	@include('main.home.partials._carousel_header')
@endsection


@section('content')
	<div class="max">
		<div class="row">
			<div class="columns span-12">
				<h1>2-in-1 symptomatic relief from Vaginal Atrophy &amp; Dryness</h1>
				<h2>Itching <br/><img src="/images/flower.svg" class="flower" alt="Replens&trade; Moisturiser Flower Image" /> <br/>Irritation <br/><img src="/images/flower.svg" class="flower" alt="Replens&trade; Moisturiser Flower Image" /> <br/>Discomfort</h2>
			</div>
		</div>

		<div class="row block">
			<div class="columns span-4 md-12 sm-12">
				<div class="upper">
					<img src="/images/home/vaginal-dryness.jpg" alt="Vaginal Dryness" />
					<h3>What is Vaginal Dryness?</h3>
				</div>
				<div class="lower">
					For millions of women, vaginal dryness is a real problem that affects and interferes with their day-to-day lives.
					<a href="/vaginal-dryness" class="fom">Find out more</a>
				</div>
			</div>
			<div class="columns span-4 md-12 sm-12">
				<div class="upper homeProduct">
					<img src="/images/home/our-products.jpg" alt-"Vaginal Dryness Products" />
					<h3>Our products</h3>
				</div>
				<div class="lower">
					Replens MD<sup>TM</sup> Vaginal Moisturiser provides immediate alleviation of dryness symptoms and helps replenish vaginal moisture.
					<a href="/our-products" class="fom">Find out more</a>
				</div>
			</div>
			<div class="columns span-4 md-12 sm-12">
				<div class="upper">
					<img src="/images/home/ask-our-experts.jpg" alt="Vaginal Dryness Products Expert Help" />
					<h3>Ask our experts</h3>
				</div>
				<div class="lower">
					Learn more about Vaginal Dryness from our product and expert FAQs.
					<a href="/product-faqs" class="fom">Find out more</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

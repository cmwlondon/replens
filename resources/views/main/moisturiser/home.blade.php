@extends('main.layouts.main')

@section('header')
	@include('main.moisturiser.partials._banner_header')
@endsection

@section('content')
<div class="row full moisturiser-content">
	<div class="columns span-12 hidden-xlarge hidden-xxlarge alt-header-text">
		<div class="row">
			<div class="columns span-10 before-1 after-1 align-center">
				<h1 class="absPos">How do I know if I need a vaginal moisturiser?</h1>
			</div>
		</div>
	</div>

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1">
					<p>Acknowledging the issue is step one, asking the right questions is step two. Understanding if you need a vaginal moisturiser is simpler than you may think and comes down to two questions:</p>
					<p class="header-question">Are you experiencing vaginal dryness and its symptoms?</p>
					<p class="header-question">Do you want to do something about it?</p>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">

			<div class="row full">
				<div class="columns span-12 white-background">
					<div class="row full pdbtm-40 pdtp-40 gradientBackground">
						<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 margin-bottom20">
							<p class="no-margin-top">Sexuality continues to be an important part of a happy and healthy life. Until very recently, healthcare companies have put most of their time and research into developing products and medicine that mainly address male sexual dysfunction – but it takes two to tango. If the goal is a better sex life, then viewing women&rsquo;s needs (like vaginal dryness) is as important as men&rsquo;s.</p>
							<p>The pharmaceutical advances for men have given them options to maintain a healthy sex life as they age, however, many of their partners still experience chronic vaginal dryness, making it difficult for women to respond to their partners’ increased expectations.</p>
							<p>It leaves many women suffering in silence, but the physical and emotional symptoms associated with vaginal dryness are how women should know that they might need a vaginal moisturiser.</p>
						</div>

						<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 align-center margin-bottom20">
							<img src="/images/moisturiser/dancing-couple.jpg" alt="Dancing Couple Image - Replens&trade; Moisturiser" />
						</div>
					</div>

					<div class="row full blue-section">
						<div class="columns span-12 margin-bottom20 border-top no-padding">
							<h2>Symptoms</h2>
						</div>

						<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 white-border">
							<p>If you've had vaginal dryness, you know how uncomfortable it can feel. Symptoms can include:
								<ul>
									<li>irritation</li>
									<li>itching</li>
									<li>burning</li>
									<li>soreness</li>
								</ul>
							</p>
						</div>

						<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 white-border">
							<p>If left untreated, this condition can lead to:
								<ul>
									<li>atrophic vaginitis</li>
									<li>urinary discomfort</li>
									<li>bladder infections</li>
									<li>painful intercourse</li>
								</ul>
							</p>
						</div>

						<div class="columns span-10 before-1 after-1">
							<p>If the signs and symptoms are there, it’s more than likely affecting your sex life. Many women put off seeking treatment often because of a reluctance to talk about the issue, despite there being effective solutions to treat vaginal dryness symptoms.</p>
						</div>
					</div>

					<div class="row full">
						<div class="columns span-12 margin-bottom20 border-top no-padding">
							<h2>Solutions</h2>
						</div>

						<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 margin-bottom20">
							<!-- <p>Acknowledging the issue is step one, asking the right questions is step two. Understanding if you need a vaginal moisturiser is simpler than you may think and comes down to two questions:</p>
								<ul>
									<li>Are you experiencing vaginal dryness and its symptoms?</li>
									<li>Do you want to do something about it?</li>
								</ul> -->
							<p>There are choices to address vaginal dryness, hormone replacement therapy is one of the most commonly used treatments, particularly for menopausal women. However, hormones are not for everyone and HRT does present potential risks. A hormone free option, such as a vaginal moisturiser, helps to provide soothing comfort and ensure good vaginal health. Unlike most other lotions and lubricant based solutions Replens MD<sup>TM</sup> acts directly to the source of the problem, by delivering continuous moisture, making each single application last up to 3 days.</p>
							<img src="/images/moisturiser/pills.jpg" alt="Vaginal Dryness Moisturiser Pills" />
						</div>

						<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1">
							<img src="/images/products/vm.png" alt="Replens&trade; Vaginal Dryness Products" />
							<p>As an in-the-moment solution, many women opt for a good lubricant when faced with the prospect of dry, painful sex. If dryness is a challenge during intercourse, a lubricant will provide great temporary assistance by supplementing the body’s natural lubrication.</p>
							<p>All of this is good news for women when their men are taking a sexual enhancement medication and it can improve the sex lives of millions of women, at any age. Additionally, it can help raise the quality of life by easing chronic discomfort.</p>
							<p>Men are discovering that while the body’s processes can easily frustrate desire, that doesn’t mean they have to like it. Neither do women, and nor should they. You are never too dry to try, as long as you know what you can do about it.</p>
						</div>

						<div class="columns span-10 before-1 after-1 align-center">
							<a href="/vaginal-dryness" class="find-out-more">
								What is vaginal dryness? <span>CLICK&nbsp;HERE&nbsp;TO&nbsp;FIND&nbsp;OUT&nbsp;MORE</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

@extends('main.layouts.main')

@section('header')
	@include('main.symptom-checker.partials._banner_header')
@endsection

@section('content')
<div class="row full symptom-checker-content">
	<div class="columns span-12 hidden-large hidden-xlarge hidden-xxlarge alt-header-text">
		<div class="row">
			<div class="columns span-10 before-1 after-1 align-center">
				<h1 class="absPos">Symptom Checker</h1>
			</div>
		</div>
	</div>

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1">
					<h1 class="symptom-checker">Experiencing itching or irritation down there? Use our simple symptom checker to help identify if you could be suffering from vaginal dryness.</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">
		  <div id="symptomApp">
				<h3>When it comes to your vagina, do you experience…</h3>
			  <symptom-checker v-for="(questionItem, index) in questionsArray"
					:key="index"
					:question-id="index"
					:data-id="index"
					:question-current="currentQuestion"
					:question="questionItem.question"
					:answer-array="questionItem.answerArray">
				</symptom-checker>
				<symptom-result></symptom-result>
				<a name="bottom"></a>
			</div>
		</div>
	</div>

	@include('main.symptom-checker.partials._symptoms_template')
</div>


@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

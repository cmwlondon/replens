<script type="x/templates" id="template-symptoms">
  <transition name="fade">
    <div v-if="isActive" class="answer-wrap">
      <h3 class="side-padding">@{{ question }}</h3>
      <div class="row answer-list" :data-row-id="id">
        <symptom-answer v-for="(answer, ind) in answerArr"
          :key="ind"
          :question-id="questionId"
          :answer-id="ind"
          :answer-len="answerLen"
          :value="answer"></symptom-answer>
      </div>
    </div>
  </transition>
</script>

<script type="x/templates" id="template-answers">
  <div :class="defineGrid">
    <a href="#"
      :data-qid="questionId"
      :data-aid="answerId"
      :data-av="$options.filters.filterNumber(value)"
      :answer-len="answerLen"
      @click="updateCurrentQuestion"
      v-html="$options.filters.filterText(value)"
      :class="selectClass"></a>
  </div>
</script>

<script type="x/templates" id="template-result">
    <div class="result-wrap">
      <a name="end"></a>
      <transition name="fade">
        <div v-if="show1">
          <div class="row">
            <div class="columns sm-up-12">
              <div class="row">
                <div class="columns sm-up-12 lg-up-8">
                  <h3>It sounds like you could be suffering from vaginal dryness.</h3>
                  <p>Follow advice below from <a href="http://www.drrosemaryleonard.co.uk" target="_blank"><span class="bold">Dr Rosemary Leonard</span></a> on Causes, Symptoms and Treatment.</p>
                  <h4>What is vaginal dryness?</h4>
                  <p>&ldquo;Vaginal dryness is a common condition that can affect women of any age, though it is most common in post – menopausal women. It occurs when there is a lack of natural secretions in the genital area, and just like dry skin anywhere else on the body, it can lead to irritation and discomfort.&rdquo;</p>

                  <div class="row full tab-secion-wrap">
                    <div class="columns sm-up-12">
                      <div class="row full">
                        <div class="columns sm-up-12 md-up-4 tab-menu padding-right-normal">
                          <div class="tab-inner highlight" @click="toggleTab" data-tab="tabOne">
                            Causes
                          </div>
                        </div>
                        <div class="columns sm-up-12 md-up-4 tab-menu padding-right-normal">
                          <div class="tab-inner" @click="toggleTab" data-tab="tabTwo">
                            Symptoms
                          </div>
                        </div>
                        <div class="columns sm-up-12 md-up-4 tab-menu">
                          <div class="tab-inner" @click="toggleTab" data-tab="tabThree">
                            Treatment
                          </div>
                        </div>
                      </div>
                      <div class="row full">
                        <div class="columns sm-up-12">
                          <div class="tab-section" v-if="showTabOne">
                            <div class="inner">
                              <h4>What are the causes?</h4>
                              <p>&ldquo;Normally, the walls of the vagina are lubricated by a thick layer of clear fluid that is secreted from the glands in the genital area. The hormone oestrogen helps stimulate production of the fluid, and also helps to keep the vaginal walls thick and elastic. The most common cause of vaginal dryness is a decrease of oestrogen most commonly caused by the menopause, but it can also occur when using tampons, especially at the end of a period. It can also occasionally occur as a side effect of some medications, and more rarely in some medical conditions.</p>
                              <p>In some circumstances, such as breast feeding, vaginal dryness is only a temporary problem. But unfortunately after the menopause there is no cure, and treatments have to be used long term. Talk to your GP for advice about long-term treatment options to find the best management routine to suit you.&rdquo;</p>
                            </div>
                          </div>
                          <div class="tab-section" v-if="showTabTwo">
                            <div class="inner">
                              <h4>What symptoms can be experienced?</h4>
                              <p>&ldquo;Most women realise their vagina is dry during intimacy. The increase in natural secretions during sexual arousal does not occur, and this can mean having intercourse is very uncomfortable. Vaginal dryness can also cause slight itching, and soreness, especially when using paper after going to the toilet.  Unlike other causes of soreness, vaginal dryness can be alleviated by using a vaginal moisturiser. Like ReplensMD<sup>TM</sup>, clinically shown to act on the source of dryness by immediately treating and soothing symptoms, replenishing intimate moisture and providing up to 3 days of long-lasting relief in one application.&rdquo;</p>
                            </div>
                          </div>
                          <div class="tab-section" v-if="showTabThree">
                            <div class="inner">
                              <h4>Treatment and prevention</h4>
                              <p>&ldquo;The best treatment for vaginal dryness depends very much on personal preference following guidance from your GP. For some HRT or an oestrogen cream is the treatment of choice. However, if you prefer something hormone-free, it is important to chose a product that has been proven in clinical studies*, such as Replens MD<sup>TM</sup> vaginal moisturiser, widely trusted by GP&rsquo;s to be effective and safe for delivering long-lasting relief from vaginal dryness symptoms.&rdquo;</p>
                              <p><small>*Replens MD<sup>TM</sup> vaginal moisturiser gel soothes and treats symptoms of vaginal dryness delivering continuous moisture for up to 3 days of immediate and long-lasting relief in just 1 application.</small></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="columns sm-up-12 lg-up-4 image-wrap">
                  <img src="/images/symptoms/moisturiser-packshot.png" alt="">
                  <br>
                  <a href="http://www.boots.com/search/replens#container" target="_blank" class="find-out-more symptom-checker-more">Buy Now</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div v-if="show2">
          <h3>We recommend visiting your doctor</h3>
          <p>No need to worry but it sounds like you could be suffering from something other than vaginal dryness. Based on your response we would recommend you see your doctor to check on the underlying cause. Your doctor will be used to discussing these types of symptoms everyday, so talk them through your symptoms for them to provide an accurate diagnosis.</p>
          <h4><a href="http://www.drrosemaryleonard.co.uk" target="_blank"><span class="bold">Dr Rosemary Leonard</span></a> says…</h4>
          <p>&ldquo;Vaginal dryness can cause discomfort and slight itching, but it does not cause the intense itching that can occur in thrush. And unlike thrush or Bacterial Vaginosis, vaginal dryness does not cause an odour or a discharge.</p>
          <p>If your vagina feels uncomfortable at any time, then you should see your doctor to check on the underlying cause. If a choice of doctors is available at your surgery, then ask the receptionist if there is one who specialises in women&rsquo;s health issues. If the thought of talking to a male doctor embarrasses you, and there is no female doctor available, then speak to your practice nurse.  It is likely that the doctor or nurse will want to examine you, so go prepared for this! Vaginal dryness is a very common condition, and your healthcare professional most likely talks to women about it on an almost weekly basis. Don&rsquo;t be embarrassed, take a deep breath and talk to them about your symptoms, it might also help to write them down to take in with you as well as any questions you may want to ask.&rdquo;</p>
        </div>

        <div v-if="show3">
          <h3>All sounds healthy</h3>
          <p>From what you&rsquo;ve told us, it sounds like your vagina is in good health. </p>
          <p>However, if you do still have concerns or if any of your symptoms change then we would recommend that you discuss these with your doctor or nurse. </p>
          <h4><a href="http://www.drrosemaryleonard.co.uk" target="_blank"><span class="bold">Dr Rosemary Leonard</span></a> says…</h4>
          <p>&ldquo;If your vagina feels uncomfortable at any time, then you should see your doctor to check on the underlying cause. If a choice of doctors is available at your surgery, then ask the receptionist if there is one who specialises in women&rsquo;s health issues. If the thought of talking to a male doctor embarrasses you, and there is no female doctor available, then speak to your practice nurse.  It is likely that the doctor or nurse will want to examine you, so go prepared for this! Don&rsquo;t be embarrassed, take a deep breath and talk to them about your symptoms, it might also help to write them down to take in with you as well as any questions you may want to ask.&rdquo;</p>
        </div>
    </transition>
    </div>
</script>

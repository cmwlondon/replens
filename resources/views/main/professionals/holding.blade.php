@extends('main.layouts.main')

@section('content')
<div class="row full contact-content holding">
  <div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
		      <h1 style="margin-top:200px;">Coming soon</h1>
				<p>You’ll soon be able to request Replens MD<sup>TM</sup> samples for use within your practice on this page. Until then, please submit your request through our contact form <a href="/contact-us" style="color:#ffffff;">here &gt;&gt;</a></p>
		    </div>
		  </div>
		</div>
	</div>

</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

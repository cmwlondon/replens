  {!! Form::open(array('url' => ['professionals#form'], 'method' => 'POST', 'id' => 'samplesForm') )!!}

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('title', 'Title') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('title', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('firstname', 'First Name') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('firstname', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('lastname', 'Last Name') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('lastname', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('email', 'Email Address') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('email', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('phone', 'Telephone') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('phone', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('jobTitle', 'Job Title') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('jobTitle', null, ['placeholder' => '', 'id' => 'jobTitle', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('jobTitle', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('practiceName', 'Practice Name') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('practiceName',null,['placeholder' => '', 'id' => 'practiceName', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('practiceName', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('practiceAddress', 'Practice Address') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        {!! Form::text('practiceAddress',null,['placeholder' => '', 'id' => 'practiceAddress', 'class' => 'input']) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('practiceAddress', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-9 sm-12 md-12">
        {!! Form::label('patients', 'As a weekly average, how many patients do you see that suffer from vaginal dryness?') !!}
      </div>
      <div class="columns span-3 sm-12 md-12">
        {!! Form::select('patients', $patientsArr, null, ['id' => 'patients', 'class' => 'input short'] ) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('patients', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-9 sm-12 md-12">
        {!! Form::label('optIn', 'Tick here if you’re happy to receive e-newsletters and relevant updates from Replens MD&trade; by email.') !!}
      </div>
      <div class="columns span-3 sm-12 md-12">
        {!! Form::checkbox('optIn', '1', null, ['id' => 'optIn', 'class' => 'chkbox'] ) !!}
      </div>
      <div class="columns span-12">
        {!! $errors->first('optIn', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-3 sm-12 md-12">
        {!! Form::label('captcha', 'I&rsquo;m a real person') !!}
      </div>
      <div class="columns span-9 sm-12 md-12">
        @captcha
        <input type="text" id="captcha" name="captcha">
      </div>
      <div class="columns span-12">
        {!! $errors->first('captcha', '<small class="error">* :message</small>') !!}
      </div>
    </div>

    

    <div class="formRow row full submitRow">
      <div class="columns span-12">
        <a href="#" class="btnSubmit" onclick="$(this).closest('form').submit()">Request Samples &gt;&gt;</a>
      </div>
    </div>

  {!! Form::close() !!}

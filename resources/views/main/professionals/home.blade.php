@extends('main.layouts.main')

@section('header')
	@include('main.professionals.partials._banner_header')
@endsection

@section('content')
<div class="row full professionals-content">
	<div class="columns span-12">
		<div class="max">
			{{--
			<div class="row full sec-0 hidden-large hidden-large hidden-xlarge hidden-xxlarge ">

				<div class="columns span-6 md-12 sm-12">
					<div class="inner"><a href="#" class="button form-btn">Request free samples<div class="arrow"></div></a></div>
				</div>
				<div class="columns span-6 md-12 sm-12">
					<div class="inner"><a href="#" class="button video-btn">View CPD video<div class="arrow"></div></a></div>
				</div>
			</div>
			--}}

			<div class="row full academy">
				<header>
					<h1>FREE CPD MODULE built in partnership with&nbsp;<img alt="HEALTH PROFESSIONAL ACADEMY" src="/images/professionals/hpa.png"></h1>
					
				</header>
				<h2>Vaginal dryness: <br>A silent problem</h2>
				<div class="whiteBox">
					<img class="clock" alt="30 - 60 mins" src="/images/professionals/clock.png">
					<h3>Learning objectives</h3>
					<ul>
						<li>Understand the causes of vaginal dryness and its impact on a woman&rsquo;s&nbsp;life</li>
						<li>Be able to recognise the common symptoms and signs of vaginal&nbsp;atrophy</li>
						<li>Feel confident discussing treatment options with&nbsp;patients</li>
					</ul>
					<h3>Click below to access the&nbsp;modules</h3>
					<div class="moduleSelect">
						<div class="msBox mba">
							<a href="https://www.healthprofessionalacademy.co.uk/mum-and-baby/learn/vaginal-dryness-a-silent-problem" target="_blank" title="MUM &amp; BABY ACADEMY"><img alt="MUM &amp; BABY ACADEMY" src="/images/professionals/mba.png"></a>
							<a href="https://www.healthprofessionalacademy.co.uk/mum-and-baby/learn/vaginal-dryness-a-silent-problem" target="_blank" class="buttonx">GP'S AND MIDWIVES&nbsp;&gt;&gt;</a>
						</div>
						<div class="msBox pa">
							<a href="https://www.healthprofessionalacademy.co.uk/pharmacy/learn/vaginal-dryness-a-silent-problem" target="_blank" title="PHARMACY ACADEMY"><img alt="PHARMACY ACADEMY" src="/images/professionals/pa.png"></a>
							<a href="https://www.healthprofessionalacademy.co.uk/pharmacy/learn/vaginal-dryness-a-silent-problem" target="_blank" class="buttonx">PHARMACISTS&nbsp;&gt;&gt;</a>
						</div>
					</div>
				</div>
			</div>

			{{--
			<div class="row full sec-1">
				<div class="columns span-6 md-12 sm-12">
					<div class="inner">
						<h1>Replenish your patient&rsquo;s confidence with the UK&rsquo;s No.1 GP choice for vaginal dryness*</h1>
						<ul>
							<li>Vaginal dryness symptoms involve itching, irritation, discomfort and painful sex</li>
							<li>1/6 of women under 50 and 1/2 over 51 will suffer vaginal dryness**</li>
							<li>The confusion around symptoms can lead patients to misdiagnosis and use of the wrong OTC treatment</li>
							<li>Only 1/4 will actually seek advice***</li>
						</ul>
					</div>
				</div>
				<div class="columns span-6 md-12 sm-12">
					<div class="inner">
						<div class="divide"></div>
						<h1>Unlike lubricants, Replens MD<sup>TM</sup> is the right solution for long lasting relief</h1>
						<ul>
							<li>Different from other lotions or lubricants thanks to the Polycarbophil gel (a bio adhesive gel that delivers continuous moisture)</li>
							<li>Clinically tested response for vaginal dryness</li>
						</ul>
						<a href="/our-products#how-does-it-work" class="button">How does Replens MD<sup>TM</sup> work? &gt;&gt;</a>
					</div>
				</div>
				<div class="columns span-12 footnotes">
					<p>*GPrX data – from December 2017 to November 2018 – UK coverage</p>
					<p>*** Cardozo L et al. Meta-analysis of estrogen therapy in the management of urogenital atrophy in postmenopausal women: second report of the Hormones and Urogenital therapy Committee. Obstet Gynecol 1998; 92:722-727</p>
				</div>
			</div>
			--}}
		</div>
	</div>

	@if (session('response'))
	  <div class="columns span-12">
			<div class="max">
				<div class="row full sec-2">
					<div class="columns span-12 response">
			      		<a name="thanks"></a>
			      		{!! session('response.message') !!}
			    	</div>
			  	</div>
			</div>
		</div>
	@else

	<div class="columns span-12">
		<div class="row full">
			<div class="columns span-12">
				<div class="max">

					{{--
					<div class="row full sec-1 video-section">
						<a name="video"></a>
						<div class="columns span-8 md-12 sm-12">
							<div class="inner">
								<h1>FREE CPD MODULE: Recognising and treating vaginal dryness</h1>
								<p>GP and menopause expert Dr Louise Newson discusses the options for effectively treating vaginal dryness</p>
								<!-- <img src="/images/professionals/video-coming-soon.jpg" class="w100 cf mt2 mb2"/> -->
								<iframe width="100%" height="100%" src="https://www.youtube-nocookie.com/embed/VYckSq4q-40?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div class="columns span-4 md-12 sm-12 objective-wrap">
							<div class="inner">
								<h1>Learning objectives</h1>
								<p>This module discusses the treatment for vaginal dryness, including:</p>
								<ul>
									<li><span>The importance of asking about vaginal dryness in the history</span></li>
									<li><span>The value of moisturisers in management</span></li>
									<li><span>When to use topical oestrogens</span></li>
									<li><span>The roll of HRT - when to use, and when to stop</span></li>
								</ul>
							</div>
						</div>
						<div class="extra-fill"></div>
					</div>
					--}}

				</div>
			</div>
		</div>

	@endif

		<div class="max">
			<div class="row full sec-2">
				<div class="columns span-8 md-12 sm-12 request-form step box-1">
					<a name="form"></a>
					<div class="inner">

						<h1>To compliment this learning, feel free to watch this video</h1>
						<div class="videoFrame">
							<iframe width="100%" height="100%" src="https://www.youtube-nocookie.com/embed/VYckSq4q-40?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>	
						</div>

						{{--
						<h1>Request samples of Replens MD<sup>TM</sup> to share with your patients in consultations:</h1>
						@include('main.professionals.partials._samples_form')
						--}}
					</div>
				</div>
				<div class="columns span-4 md-12 sm-12 useful-downloads step box-2">
					<div class="inner">
						<div class="divide"></div>
						<h1>Replens MD<sup>TM</sup> patient information leaflet</h1>
						<img src="/images/professionals/replens-gp-leaflet-22102019.jpg" style="width:168px;height:auto;" class="cf mt2"/>
						<p class="mb2">Replens MD<sup>TM</sup> <br>information leaflet</p>
						<a href="/professionals/download/leaflet" class="button">Download&nbsp;&gt;&gt;</a>
					</div>
				</div>
			</div>
		</div>

</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

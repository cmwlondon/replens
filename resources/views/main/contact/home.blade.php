@extends('main.layouts.main')

@section('content')
<div class="row full contact-content">
	@if (session('response'))
	  <div class="columns span-12 primary-color-header">
			<div class="max">
				<div class="row full">
					<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
			      <h1>{!! session('response.message') !!}</h1>
			    </div>
			  </div>
			</div>
		</div>
	@else
		<div class="columns span-12 primary-color-header">
			<div class="max">
				<div class="row full">
					<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
						<h1>Contact Us</h1>
						<p>If you have a comment or question about Replens MD<sup>TM</sup> please complete our enquiry form below.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="columns span-12 main-body">
			<div class="max">
				<div class="row full">
					<div class="columns span-6 before-3 after-3 sm-10 sm-before-1 sm-after-1">
						@include('main.contact.partials._contact_form')
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

<a name="form"></a>

  {!! Form::open(array('url' => ['contact-us#form'], 'method' => 'POST', 'id' => 'contactForm') )!!}

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('title', 'Title: *') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::select('title', $titlesArr, null, ['id' => 'title', 'class' => 'input short'] ) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('title', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('firstname', 'First Name: *') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('firstname', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('lastname', 'Last Name: *') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('lastname',null,['placeholder' => '', 'id' => 'lastname', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('lastname', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <?php /*
    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('address', 'Address:') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('address',null,['placeholder' => '', 'id' => 'address', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('address', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('city', 'City:') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('city',null,['placeholder' => '', 'id' => 'city', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('city', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('postcode', 'Postcode:') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('postcode',null,['placeholder' => '', 'id' => 'postcode', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('postcode', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('country', 'Country:') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('country',null,['placeholder' => '', 'id' => 'country', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('country', '<small class="error">:message</small>') !!}
      </div>
    </div>
    */ ?>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('phone', 'Phone Number:') !!}
      </div>
      <div class="form columns span-8 sm-12 md-12">
        {!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('phone', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('email', 'Email Address: *') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('email', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('product', 'Product:') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::text('product',null,['placeholder' => '', 'id' => 'product', 'class' => 'input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('product', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-4 sm-12 md-12">
        {!! Form::label('comments', 'Comments:') !!}
      </div>
      <div class="columns span-8 sm-12 md-12">
        {!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'textarea-input']) !!}
      </div>
      <div class="columns span-8 before-4 sm-12 sm-before-0 md-12 md-before-0">
        {!! $errors->first('comments', '<small class="error">:message</small>') !!}
      </div>
    </div>

    <div class="formRow row full">
      <div class="columns span-12 sm-12 md-12 align-right">
        <a href="#" class="submit-a" onclick="$(this).closest('form').submit()"><i class="demo-icon icon-submit"></i></a>
      </div>
    </div>

  {!! Form::close() !!}

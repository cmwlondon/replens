@extends('main.layouts.main')


@section('header')
	@include('main.vaginal.partials._banner_header')
@endsection


@section('content')
<div class="row full vaginal-content">
	<div class="columns span-12 hidden-large hidden-xlarge hidden-xxlarge alt-header-text">
		<div class="row">
			<div class="columns span-10 before-1 after-1 align-center">
				<h1 class="absPos">What is Vaginal Dryness and how is it caused?</h1>
			</div>
		</div>
	</div>

	<div class="columns span-12 primary-color-header">
		<div class="max">
			<div class="row full">
				<div class="columns span-10 before-1 after-1">
					<p>For millions of women, vaginal dryness is a real problem that affects and interferes with their day-to-day lives. Vaginal dryness is common and can occur at any age. The effects of vaginal dryness vary from woman to woman, ranging from minor discomfort to painful sexual intercourse. Regardless of the level of discomfort, vaginal dryness can impact your daily life.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="columns span-12 main-body">
		<div class="max">

			<div class="row full pdbtm-20 gradientBackground pdtp-40">
				<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 margin-bottom20">
					<p class="no-margin-top">Vaginal Dryness is usually associated only with menopause. Why? Because at that time oestrogen levels drop and one of oestrogen&rsquo;s roles is to keep the lining of the vagina moist and healthy.</p>
					<p>In addition to menopause, vaginal dryness can be associated with breastfeeding or taking oral contraceptives. It can also occur in response to douching, pelvic radiation, prolonged use of tampons, advanced endometriosis, and as a side effect of a variety of medications, including aromatase inhibitors used to treat breast cancer.</p>
					<p>Excessive exercise, persistent emotional stress or past traumatic sexual experiences, diuretics such as alcohol and caffeine, cigarette smoking, certain allergy medications and antihistamines can also exacerbate an existing condition.</p>
				</div>

				<div class="columns span-4 before-1 after-1 md-10 md-before-1 md-after-1 sm-10 sm-before-1 sm-after-1 align-center margin-bottom20">
					<img src="/images/vaginal/vaginal-lady.jpg" alt="Replens MD&trade; Moisturiser Lady Image" />
				</div>
			</div>

			<div class="row full white-background pdtp-20 pdbtm-120 border-top">
				<div class="columns span-6 sm-10 sm-before-1 sm-after-1 md-10 md-before-1 md-after-1 margin-bottom20 border-left">
					<div class="inner">
						<h2>Diabetes</h2>
						<p>Women living with diabetes are more likely to experience sexual issues, but they often suffer in silence. Potential reasons for the increased prevalence among diabetic women include changes in the vagina due to functional vascular changes, neuropathy, or tissue glycosylation related to diabetes and side effects from diabetic medications. One of the most common concerns is vaginal dryness.
							<ul>
								<li>Women with diabetes have a 33% increased incidence of vaginal dryness</li>
								<li>Up to 42% of women with diabetes experience sexual dysfunction</li>
								<li>The odds of a diabetic woman having painful intercourse are 3.48 times greater (per a study of postmenopausal women)</li>
							</ul>
						</p>
						<p><span class="primary-colour">Replens MD<sup>TM</sup>  Vaginal Moisturiser is the number one vaginal moisturiser GP recommended brand* and is an excellent, oestrogen free, alternative to Hormone Treatment for vaginal dryness.</span></p>
						<p>Replens MD<sup>TM</sup> does not treat diabetes. Please talk to your doctor about any concerns and for medical advice.</p>
						<p class="small">*GPrX data – from January 2019 to December 2019 – UK coverage</p>
					</div>
				</div>

				<div class="columns span-6 sm-10 sm-before-1 sm-after-1 md-10 md-before-1 md-after-1">
					<div class="inner">
						<h2>Menopause</h2>
						<p>Menopause is a natural transition that occurs as women leave their childbearing years. As early as our mid-30s, oestrogen levels can begin to decline causing hot flashes, night sweats, sleep disturbances, mood swings and vaginal dryness. Over time, most symptoms, like hot flashes and night sweats, will go away, but lower oestrogen levels and its effect on skin will continue for the remainder of life. Vaginal dryness continues past menopause and tends to worsen over time.
							<br><br>Replens MD<sup>TM</sup> is especially helpful in this time of life as it helps to replenish vaginal moisture by attaching to dry, compacted cells and delivering continuous moisture. This allows the vaginal tissues to absorb moisture leaving them hydrated and rejuvenated.
							<br><br>Vaginal dryness, whether mild or severe, can cause pain during intimacy and discomfort, itching and irritation even among those who are not sexually active. This condition can affect menopausal women and continue to for the remainder of their life.
							<br><br>For more information please visit the <a class="primary-color"href="http://www.nhs.uk/Conditions/dry-vagina/Pages/Introduction.aspx" target="_blank">NHS website</a>.</p>
					</div>
				</div>

				<div class="columns span-10 before-1 after-1 align-center">
					<a href="/vaginal-moisturiser" class="find-out-more">
							How do I know if I need a vaginal moisturiser? <span>CLICK&nbsp;HERE&nbsp;TO&nbsp;FIND&nbsp;OUT&nbsp;MORE</span>
					</a>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection

@section('footer')
	@include('main.layouts.partials._footer')
@endsection

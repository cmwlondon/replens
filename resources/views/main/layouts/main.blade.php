<!doctype html>
<html class="no-js" lang="en">
	<head>
<!-- OneTrust Cookies Consent Notice start for replens.co.uk -->
<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="9e3a1631-7ff9-4375-8164-27ab0c706ca3" ></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- OneTrust Cookies Consent Notice end for replens.co.uk -->
		<meta charset="utf-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1" />


		<title>{!! $meta['title'] !!}</title>
		<meta name="description" content="{{{$meta['desc']}}}" />
		<meta name="keywords" content="{{{ isset($meta['keywords']) ? $meta['keywords']	: '' }}}" />
		<meta name="keyphrases" content="{{{ isset($meta['keyphrases']) ? $meta['keyphrases']	: '' }}}" />
		<meta name="robots" content="INDEX, FOLLOW" />

		<link rel="stylesheet" href="/css/main/styles.css?{!!getenv('CACHE_BUSTER')!!}" toRefresh="/css/main/styles.css"/>

		@if (isset($pageViewCSS) && $pageViewCSS != '')
			<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css?{!!getenv('CACHE_BUSTER')!!}" toRefresh="/css/{{{$pageViewCSS}}}.css"/>
		@endif

		<link rel="canonical" href="{{{$meta['link']}}}" />
		<meta property="og:locale" content="en_GB" />
		<meta property="og:type" content="{{{$meta['pagetype']}}}" />
		<meta property="og:title" content="{{{$meta['title']}}}" />
		<meta property="og:description" content="{{{$meta['desc']}}}" />
		<meta property="og:url" content="{{{$meta['link']}}}" />
		<meta property="og:site_name" content="Replens" />
		<meta property="og:image" content="{{{$meta['image']}}}" />

		@include('main.layouts.partials._favicon')

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/animations-ie-fix.css">
			<style type="text/css">
			    #switch-group {display:none;}
			</style>
		<![endif]-->

		<!--[if gte IE 9]>
		  <style type="text/css">
		    .gradient {
		       filter: none!important;
		    }
		  </style>
		<![endif]-->
	</head>

	<body>
		<a name="top"></a>

		{{-- cookie banner START --}}
		{{--
		<div class="cookie-disclaimer">To give you the best possible experience this site uses cookies. By continuing to use this website you are giving consent to cookies being used. For more information visit our <a href="/cookie-notice">Cookie&nbsp;Notice</a>.<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 64.6 64.6" enable-background="new 0 0 64.6 64.6" xml:space="preserve" class="btn-close">
			    <line x1="3.5" y1="3.5" x2="61.1" y2="61.1"/>
			    <line x1="61.1" y1="3.5" x2="3.5" y2="61.1"/>
			</svg></div>
		--}}
		{{-- cookie banner END --}}

		<div id="wrapper">
			<div id="mobile-white-out" class="hidden"></div>
			<div id="headerWrapper">
				<div class="max">

					<div class="upper-menu"><a href="/">Home</a> | <a href="/contact-us">Contact us</a></div>
					<a href="/professionals" class="btnProfessionals">Professionals</a>
					<a href="#"><img src="/images/hamburger.svg" class="burger"/></a>
				</div>

				<div class="slide-menu">
					<img src="/images/close.svg" class="close"/>
					<div class="scroll-menu no-scrollbar">
						<menu>
							<li><a href="/">Home</a></li>
							<li>Vaginal Dryness
								<ul class="sub-menu width-2">
									<li><a href="/vaginal-dryness">What is vaginal dryness and how is it caused?</a></li>
									<li><a href="/vaginal-moisturiser">How do I know if I need a vaginal moisturiser?</a></li>
									<li><a href="/symptom-checker">Symptom checker</a></li>
								</ul>
							</li>
							<li><a href="/our-products">Our Products</a></li>
							<li>FAQs
								<ul class="sub-menu width-2">
									<li><a href="/product-faqs">Product FAQs</a></li>
								</ul>
							</li>
							<li><a href="/contact-us">Contact us</a></li>
							<li><a href="/professionals">Professionals</a></li>
						</menu>
					</div>
					<div class="slide-menu-border"></div>
				</div>

			</div>
			<div id="menuWrapper">

				@yield('header')

				<div class="swoosh-over"></div>
				<svg version="1.1" class="swoosh-curve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
		 viewBox="0 0 1920 295" xml:space="preserve" preserveAspectRatio="none">
					<path d="M0,0h1920v90.5c0,0-227.5-16.4-515.4-16.4C503.1,74.1,0,295,0,295L0,0z"/>
				</svg>

				<div class="upperMenuWrapper">
					<a href="/"><img src="/images/replens-logo.png" class="logo" alt="Replens&trade; Logo"/></a>
					<img src="/images/gp-recommended-brand.svg" class="gp-brand" alt="GP Recommended Brand Moisturiser"/>
					<menu>
						<li><a href="#">Vaginal Dryness</a>
							<ul class="sub-menu width-2">
								<li><a href="/vaginal-dryness">What is vaginal dryness and how is it caused?</a></li>
								<li><a href="/vaginal-moisturiser">How do I know if I need a vaginal moisturiser?</a></li>
								<li><a href="/symptom-checker">Symptom checker</a></li>
							</ul>
						</li>
						<li><a href="/our-products">Our Products</a></li>
						<li><a href="/product-faqs">FAQs</a>
							<!-- <ul class="sub-menu width-6">
								<li><a href="/expert-faqs">Expert FAQs</a></li>
								<li><a href="/product-faqs">Product FAQs</a></li>
							</ul> -->
						</li>
						<li><a href="/contact-us">Contact us</a></li>
					</menu>
				</div>
			</div>

			<div id="contentWrapper" class="{{{ $class_section }}}">
				<a name="content-section"></a>
				@yield('content')
			</div>
			<div id="footerWrapper">
				<div class="max">
					<div class="row">
						<div class="columns span-6 xlg-7 lg-7 md-8 sm-12 mb0 links">
								<p class="hidden-xsmall hidden-small hidden-medium"><a href="/terms-and-conditions">Terms and conditions</a> | <a href="/cookie-notice">Cookie Notice</a> | <a href="/privacy-policy">Privacy Policy</a> | <a href="/contact-us">Contact&nbsp;us</a>
									<br><span class="gp-style">*GPrX data – from January 2019 to December 2019 – UK coverage</span></p>
								<p class="increse-lineheight hidden-large hidden-xlarge hidden-xxlarge"><a href="/terms-and-conditions">Terms and conditions</a></p>
								<p class="increse-lineheight hidden-large hidden-xlarge hidden-xxlarge"><a href="/cookie-notice">Cookie Notice</a></p>
								<p class="increse-lineheight hidden-large hidden-xlarge hidden-xxlarge"><a href="/privacy-policy">Privacy Policy</a></p>
								<p class="increse-lineheight hidden-large hidden-xlarge hidden-xxlarge"><a href="/contact-us">Contact us</a></p>
								<p class="increse-lineheight hidden-large hidden-xlarge hidden-xxlarge"><span class="gp-style">*GPrX data – from January 2019 to December 2019 – UK coverage</span></p>
						</div>
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->
						<a href="/"><img src="/images/replens-logo.png" class="logo" alt="Replens&trade; Logo"/></a>
					</div>


				</div>
			</div>
		</div>
		<script>
			var site_url = "{{ $site_url }}";
			@if (isset($jsVars))
				@foreach ($jsVars as $key => $var)
					var {{$key}} = "{{ $var }}";
				@endforeach
			@endif
			@if (isset($pageViewJS))
				var pageViewJS = "{{ $pageViewJS }}";
			@endif
		</script>

		{!! Html::script('js/libs/requirejs/require.js', ['data-main' => '/js/main/main.min.js?1']) !!}

		@if (App::environment() == 'production')
			<!-- Google Analytics -->
			<script type="text/plain" class="optanon-category-2">
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function() {
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-83368525-1', 'auto');
			  ga('send', 'pageview');

			</script>
		@endif
	</body>
</html>
